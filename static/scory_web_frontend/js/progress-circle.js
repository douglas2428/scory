//By Kevin Le - http://stackoverflow.com/users/1244013/khnle
//v1.0

angular.module('ui.bootstrap.progresscircle', [])

.constant('progressConfig', {
  outerCircleWidth: 10,
  outerCircleRadius: 100,
  outerCircleBackgroundColor: '#d9d9d9',
  max: 60,
})

.controller('ProgressCircleController', ['$scope', '$attrs', '$timeout', 'progressConfig', function($scope, $attrs, $timeout, progressConfig) {
//just a place holder for now  
}])

.directive('progressCircle', ['$parse', 'progressConfig', function(parse, progressConfig) {
  return {
    restrict: 'E',
    controller: 'ProgressCircleController',
    link: function(s, e, a, progressCtrl) {
      
      var width = a.elementWidth || progressConfig.elementWidth,
          height = a.elementHeight || progressConfig.elementHeight,
          outerCircleWidth = a.outerCircleWidth || progressConfig.outerCircleWidth,
          innerCircleWidth = a.innerCircleWidth || progressConfig.innerCircleWidth,
          outerCircleBackgroundColor = a.outerCircleBackgroundColor || progressConfig.outerCircleBackgroundColor,
          outerCircleForegroundColor = a.outerCircleForegroundColor || progressConfig.outerCircleForegroundColor,
          outerCircleRadius = a.outerCircleRadius || progressConfig.outerCircleRadius,
          innerCircleRadius = a.innerCircleRadius || progressConfig.innerCircleRadius,
          percentFormat = a.percentFormat || progressConfig.percentFormat,
          displayPercentSign = a.displayPercentSign || progressConfig.displayPercentSign,
          max = a.max || progressConfig.max,
          displayMax = a.displayMax || progressConfig.displayMax;
          
      var canvas = angular.element('<canvas>').attr({'width': width, 'height': height});
      
      e.replaceWith(canvas);
      
      s.$watch(a.progressCircleModel, function(newValue) {
        // Create the content of the canvas
        var ctx = canvas[0].getContext('2d');
        ctx.clearRect(0, 0, width, height);

        // The "background" circle
        var x = width / 2;
        var y = height / 2;
        ctx.beginPath();
        ctx.arc(x, y, outerCircleRadius, 0, Math.PI * 2, false);
        ctx.lineWidth = outerCircleWidth;
        ctx.strokeStyle = outerCircleBackgroundColor;
        ctx.stroke();

        // The "foreground" circle
        var startAngle = - (Math.PI / 2);
        var endAngle = (percentFormat===true) ? ((Math.PI * 2 ) * (newValue.value/100)) - (Math.PI / 2):
          ((Math.PI * 2 ) * (newValue.value/max)) - (Math.PI / 2);
        var anticlockwise = false;
        ctx.beginPath();
        ctx.arc(x, y, outerCircleRadius, startAngle, endAngle, anticlockwise);
        ctx.lineWidth = outerCircleWidth;
        ctx.strokeStyle = outerCircleForegroundColor;
        ctx.stroke();
      }, true);
    }
  };  
}]);