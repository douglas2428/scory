appAcount = angular.module("appAcounts", ['ngBootbox','ngRoute','ui.bootstrap','ngAnimate']);
 
// ngRoute
appAcount.config(['$routeProvider', 
  function($routeProvider) {
    $routeProvider

    .when('/admin_login', {
        templateUrl: '/scory/admin_login',
    })

    .when('/admin_register', {
        templateUrl: '/scory/admin_register',
    })

    .when('/admin_recover_password', {
        templateUrl: '/scory/admin_recover_password',
    })

    .when('/admin_add_code', {
        templateUrl: '/scory/admin_add_code',
    })

    .when('/admin_new_password', {
        templateUrl: '/scory/admin_new_password',
    })
    
    .otherwise({
        redirectTo: '/admin_login'
    });
  }
]);

appAcount.config(['$httpProvider', function($httpProvider) {
    $httpProvider.defaults.xsrfCookieName = 'csrftoken';
    $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';
}]);

appAcount.config(['$qProvider', function ($qProvider) {
    $qProvider.errorOnUnhandledRejections(false);
}]);

appAcount.animation('.fade', function() {
  return {
    enter: function(element, done) {
      element.css('display', 'none');
      $(element).fadeIn(500, function() {
        done();
      });
    },
    leave: function(element, done) {
      $(element).fadeOut(500, function() {
        done();
      });
    }
  }
});

appAcount.directive('tooltip', function(){
    return {
        restrict: 'A',
        link: function(scope, element, attrs){
            $(element).hover(function(){
                // on mouseenter
                $(element).tooltip('show');
            }, function(){
                // on mouseleave
                $(element).tooltip('hide');
            });
        }
    };
});

appAcount.filter('convertToDate', function() {
    return function(str){
        return new Date(str);
    };
});
