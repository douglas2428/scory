app = angular.module("scoryApp", ['ngBootbox','ngRoute','dndLists','ngSanitize','ngAnimate','ui.bootstrap','angucomplete-alt','ngclipboard','ui.bootstrap.progresscircle']);
 
// ngRoute
app.config(['$routeProvider', 
  function($routeProvider) {
    $routeProvider

    /*.when('/admin_dashboard', {
        templateUrl: '/scory/admin_dashboard',
        activetab: 'dashboard'
    })*/

    .when('/admin_surveys', {
        templateUrl: '/scory/admin_surveys',
        activetab: 'adminSurveys'
    })

    .when('/admin_configuration', {
        templateUrl: '/scory/admin_configuration',
        activetab: 'configuration'
    })

    .when('/survey_edit/:survey', {
        templateUrl: '/scory/survey_edit',
        activetab: 'editSurvey'
    })

    .when('/survey_config/:survey', {
        templateUrl: '/scory/survey_config',
        activetab: 'configSurvey'
    })
    
    .otherwise({
        //redirectTo: '/admin_dashboard'
        redirectTo: '/admin_surveys'
    });
  }
]);

app.config(['$httpProvider', function($httpProvider) {
    $httpProvider.defaults.xsrfCookieName = 'csrftoken';
    $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';
}]);

app.config(['$qProvider', function ($qProvider) {
    $qProvider.errorOnUnhandledRejections(false);
}]);

app.animation('.fade', function() {
  return {
    enter: function(element, done) {
      element.css('display', 'none');
      $(element).fadeIn(250, function() {
        done();
      });
    },
    leave: function(element, done) {
      $(element).fadeOut(250, function() {
        done();
      });
    }
  }
});

app.animation('.fadeTrigger', function() {
  return {
    enter: function(element, done) {
      element.css('display', 'none');
      $(element).fadeIn(50, function() {
        done();
      });
    },
    leave: function(element, done) {
      $(element).fadeOut(50, function() {
        done();
      });
    }
  }
});

app.factory("Service", function() {
  return {
    data: {}
  };
});

app.directive('tooltip', function(){
    return {
        restrict: 'A',
        link: function(scope, element, attrs){
            $(element).hover(function(){
                // on mouseenter
                $(element).tooltip('show');
            }, function(){
                // on mouseleave
                $(element).tooltip('hide');
            });
        }
    };
});

app.filter('convertToDate', function() {
    return function(str){
        return new Date(str);
    };
});