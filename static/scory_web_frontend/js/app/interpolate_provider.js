// To avoid conflict with django tag {{}}. This change the access variable angular from {{variable}} to [[variable]]
app.config(function($interpolateProvider) {
    $interpolateProvider.startSymbol('[[');
	$interpolateProvider.endSymbol(']]');
});
