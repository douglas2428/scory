app.controller('CreateSurveyController', function($scope, $route, $ngBootbox, $http, $uibModal, $routeParams, $window, $timeout, $location, Service, $q) {

    $scope.load                                   = load;
    $scope.surveyNavBar                           = surveyNavBar;
    $scope.getHostUrl                             = getHostUrl;
    $scope.validSurveyId                          = validSurveyId;
    $scope.setPathEmoji                           = setPathEmoji;
    $scope.loadEmojis                             = loadEmojis;
    $scope.loadTypes                              = loadTypes;
    $scope.loadOptionQuestion                     = loadOptionQuestion;
    $scope.loadOptionsTrigger                     = loadOptionsTrigger;
    $scope.clearListOptionAnswers                 = clearListOptionAnswers;
    $scope.loadOptionAnswers                      = loadOptionAnswers;
    $scope.loadOptionAnswersDuplicated            = loadOptionAnswersDuplicated;
    $scope.loadAllOptionAnswer                    = loadAllOptionAnswer;
    $scope.removeOptionsAnswersDuplicates         = removeOptionsAnswersDuplicates;
    $scope.findOriginal                           = findOriginal;
    $scope.validQuestionText                      = validQuestionText;
    $scope.cancelItem                             = cancelItem;
    $scope.editItem                               = editItem;
    $scope.duplicateItem                          = duplicateItem;
    $scope.removeItem                             = removeItem;
    $scope.listingOptionAnswerByOperation         = listingOptionAnswerByOperation;
    $scope.verifyExistenceOptionAnswer            = verifyExistenceOptionAnswer;
    $scope.verifyOptionAnswerWithoutPreviousSaved = verifyOptionAnswerWithoutPreviousSaved;
    $scope.contOptionAnswerByOptionQuestion       = contOptionAnswerByOptionQuestion;
    $scope.saveOptionQuestion                     = saveOptionQuestion;
    $scope.onDragEnd                              = onDragEnd;
    $scope.logListEvent                           = logListEvent;
    $scope.optionQuestionOpen                     = optionQuestionOpen;
    $scope.cancelUpdateSequenceList               = cancelUpdateSequenceList;
    $scope.loadInitialOptionAnswers               = loadInitialOptionAnswers;
    $scope.addOptionAnswer                        = addOptionAnswer;
    $scope.deleteOptionAnswer                     = deleteOptionAnswer;
    $scope.deleteOptionAnswerNew                  = deleteOptionAnswerNew;
    $scope.changeStatus                           = changeStatus;
    $scope.openMessenger                          = openMessenger;
    $scope.downloadCodeMessenger                  = downloadCodeMessenger;
    $scope.openModalFormShare                     = openModalFormShare;
    $scope.showMessage                            = showMessage;
    $scope.openMessage                            = openMessage;
    $scope.openModalForm                          = openModalForm;
    $scope.dismissAlert                           = dismissAlert;
    $scope.showAlert                              = showAlert;
    $scope.cancelMessage                          = cancelMessage;
    $scope.editMessage                            = editMessage;
    $scope.saveMessage                            = saveMessage;
    $scope.jumpers                                = jumpers;
    $scope.cancelItemTrigger                      = cancelItemTrigger;
    $scope.editItemTrigger                        = editItemTrigger;
    $scope.removeItemTrigger                      = removeItemTrigger;
    $scope.saveOptionQuestionTrigger              = saveOptionQuestionTrigger;

 
    $scope.models = {
        selected: null,
        lists: {"Emojis":[], "Types":[], "OptionQuestion":[], "OptionQuestionTrigger":[], "Copies":[], "OptionAnswersAll":[], "OptionAnswers":[], "OptionAnswersNew":[], "OptionAnswersToComplete":[], "OriginalOptionAnswers":[], "OptionsAnswerSave":[], "OptionsAnswerDelete":[], "OptionsAnswerSaveDuplicate":[], "OptionsTrigger":[]}
    };

    $scope.changesOcurred = false;
    $scope.editMessageEnd = false;
    $scope.editMessageStart = false;

    function load() {
        $scope.models.lists.OptionQuestion = [];
        if (Service.data.alert){
            $scope.alertSurvey = Service.data.alert;
            showAlert("alert-success","<strong>¡Felicitaciones!</strong> La encuesta se ha guardado correctamente.", null);
            Service.data.alert = false;
        }
        $scope.surveyCurrent = $routeParams.survey;
        validSurveyId();
        loadTypes();
        loadEmojis();
        getHostUrl();


        $scope.models.lists.Copies = [];
        loadOptionsTrigger();
    }

    function surveyNavBar() {
        try {
            $scope.activeTab = $route.current.$$route.activetab;
            if ($routeParams.survey)
                return true;
        } catch (error) {
            console.log(error);
        }
        
        return false;
    }

    function getHostUrl() {
        if ($location.port())
            $scope.host = $location.protocol()+'://'+$location.host()+':'+$location.port();
        else
            $scope.host = $location.protocol()+'://'+$location.host();
    }

    // Generate initial lists Types
    function validSurveyId() {
        $http.get('/scory/api/survey/')
            .then(function success(response) {
                valid = false;
                _.each(response.data, function (survey, index) {
                    if(survey.pk == $scope.surveyCurrent){
                        valid = true;
                        $scope.surveyName = survey.name;
                        $scope.surveyCreatedAt = survey.created_at;
                        $scope.surveyUpdatedAt = survey.updated_at;
                        $scope.survey = survey;
                    }
                });
                if (!valid){
                    $scope.message = 'No se ha encontrado una encuesta con id '+$scope.surveyCurrent+'.';
                    openMessage();
                    $window.location.href = "#!/admin_dashboard/";
                }
            }).catch(function error(response) {
                    alert('error');
            });
    }

    function setPathEmoji(InitialOptionAnswer, route) {
        InitialOptionAnswer.urlImg = route;
    }

    // Generate initial lists Emojis
    function loadEmojis() {
        $scope.models.lists.Emojis = [];
        $scope.models.lists.Emojis.push({tittle: 'Ninguno', pathImg: '/static/scory_web_frontend/img/emoji.png'});
        $scope.models.lists.Emojis.push({tittle: 'Enojado', pathImg: '/static/scory_web_frontend/img/1_muy_insatisfecho.png'});
        $scope.models.lists.Emojis.push({tittle: 'Decepcionado', pathImg: '/static/scory_web_frontend/img/2_insatisfecho.png'});
        $scope.models.lists.Emojis.push({tittle: 'Neutro', pathImg: '/static/scory_web_frontend/img/3_neutral.png'});
        $scope.models.lists.Emojis.push({tittle: 'Contento', pathImg: '/static/scory_web_frontend/img/4_satisfecho.png'});
        $scope.models.lists.Emojis.push({tittle: 'Feliz', pathImg: '/static/scory_web_frontend/img/5_muy_satisfecho.png'});
    }

    // Generate initial lists Types
    function loadTypes() {
        $scope.models.lists.Types = [];
        $http.get('/scory/api/type/')
            .then(function success(response) {
                _.times(response.data['length'], function(i){
                    var type = response.data[i];
                    $scope.models.lists.Types.push({
                        type: type,
                        status: 'not saved'});
                });
            }).catch(function error(response) {
                    alert('error');
            });
    }

    // Generate initial model OptionQuestion
    function loadOptionQuestion() {
        loadAllOptionAnswer().then(function(data) {
            $scope.models.lists.OptionQuestion = [];
            $http.get('/scory/api/survey/'+$scope.surveyCurrent+'/option_question/')
                .then(function success(response) {
                    _.each(response.data, function (value, index) {
                        $scope.models.lists.OptionAnswersByOptionQuestion = [];
                        if (value.options_answer){
                            _.each($scope.models.lists.OptionAnswersAll, function (options_answer, index) {
                                _.each(value.options_answer, function (id) {
                                    if (id == options_answer.id){
                                        $scope.models.lists.OptionAnswersByOptionQuestion.push(options_answer);
                                    }
                                });
                            });
                        }
                        value.optionsAnswer = $scope.models.lists.OptionAnswersByOptionQuestion;
                        value.status = 'saved';
                        $scope.models.lists.OptionQuestion.push(value);
                    });
                    loadOriginTrigger();
                }).catch(function error(response) {
                        alert('error');
                });
            })
        .catch(function(err) {
            alert('error promise');
        });   
    }
    
    function clearListOptionAnswers() {
        $scope.models.lists.OptionAnswers = [];
        $scope.models.lists.OriginalOptionAnswers = [];
        $scope.models.lists.OptionAnswersNew = [];
        $scope.models.lists.OptionAnswersToComplete = [];
        $scope.models.lists.OptionAnswersAll = [];
        $scope.models.lists.OptionsAnswerSaveDuplicate = [];
    }
    
    function loadOptionAnswers(option_question) {
        clearListOptionAnswers();
        $http.get('/scory/api/option_question/'+option_question.id+'/options_answer/')
            .then(function success(response) {
                _.each(response.data, function (option_answer, index) {
                    findTriggerQuestion = false;
                    _.each($scope.models.lists.OptionQuestionTrigger, function (option_question_jump, index) {
                        if (option_question_jump.id === option_answer.trigger_question){
                            findTriggerQuestion = true;
                            $scope.models.lists.OptionAnswers.push({id: option_answer.pk, name: option_answer.name, urlImg: option_answer.url_img, triggerQuestion: option_question_jump, operation: ''});
                        }
                    });
                    if (!findTriggerQuestion)
                        $scope.models.lists.OptionAnswers.push({id: option_answer.pk, name: option_answer.name, urlImg: option_answer.url_img, triggerQuestion: "", operation: ''});
                });
                $scope.models.lists.OriginalOptionAnswers = angular.copy($scope.models.lists.OptionAnswers);
            }).catch(function error(response) {
                    alert('error');
            });
    }

    function loadOptionAnswersDuplicated(option_question) {
        $scope.models.lists.OptionsAnswerSaveDuplicate = [];
        $http.get('/scory/api/option_question/'+option_question.id+'/options_answer/')
            .then(function success(response) {
                _.each(response.data, function (option_answer, index) {
                    findTriggerQuestion = false;
                    _.each($scope.models.lists.OptionQuestionTrigger, function (option_question_jump, index) {
                        if (option_question_jump.id === option_answer.trigger_question){
                            findTriggerQuestion = true;
                            $scope.models.lists.OptionsAnswerSaveDuplicate.push({id:option_answer.pk, name: option_answer.name, url_img: option_answer.url_img, trigger_question: option_question_jump, operation:'duplicating'});
                        }
                    });
                    if (!findTriggerQuestion)
                        $scope.models.lists.OptionsAnswerSaveDuplicate.push({id:option_answer.pk, name: option_answer.name, url_img: option_answer.url_img, trigger_question: '', operation:'duplicating'});
                });
            }).catch(function error(response) {
                    alert('error');
            });
    }

    function loadAllOptionAnswer() {
        var defered = $q.defer();
        var promise = defered.promise;

        $scope.clearListOptionAnswers();
        $http.get('/scory/api/options_answer/')
            .then(function success(response) {
                _.each(response.data, function (option_answer, index) {
                    findTriggerQuestion = false;
                    _.each($scope.models.lists.OptionQuestionTrigger, function (option_question_jump, index) {
                        if (option_question_jump.id === option_answer.trigger_question){
                            findTriggerQuestion = true;
                            $scope.models.lists.OptionAnswersToComplete.push({id:option_answer.pk, name: option_answer.name, urlImg: option_answer.url_img, triggerQuestion: option_question_jump});
                            $scope.models.lists.OptionAnswersAll.push({id:option_answer.pk, name: option_answer.name, urlImg: option_answer.url_img, triggerQuestion: option_question_jump});
                        }
                    });
                    if (!findTriggerQuestion){
                        $scope.models.lists.OptionAnswersToComplete.push({id:option_answer.pk, name: option_answer.name, urlImg: option_answer.url_img, triggerQuestion: ''});
                        $scope.models.lists.OptionAnswersAll.push({id:option_answer.pk, name: option_answer.name, urlImg: option_answer.url_img, triggerQuestion: ''});
                    }
                });
                removeOptionsAnswersDuplicates();
                defered.resolve(response);
            }).catch(function error(response) {
                    alert('error');
                    defered.reject(response);
        });
        return promise;
    }

    function removeOptionsAnswersDuplicates() {
        var uniqueOptionsAnswers = _.uniq($scope.models.lists.OptionAnswersToComplete, 'name');
        $scope.models.lists.OptionAnswersToComplete = [];
        angular.forEach(uniqueOptionsAnswers, function(item){
            $scope.models.lists.OptionAnswersToComplete.push(item);
        });
    }

    /************************************************************************************/
    /*************************************ACTIONS OPTION-QUESTION OF SURVEY**************/
    $scope.originalData = {};

    function findOriginal(index) {
        for (var i = $scope.models.lists.Copies['length']-1; i>=0; i--) {
            var data = $scope.models.lists.Copies[i];
            if (data['referencia'] == 'index'+index)
                return data['object'];
        }
    }
    
    function validQuestionText(question_text, index){
        var questionTextValid = true;
        _.times($scope.models.lists.OptionQuestion['length'], function(i){
            var data = $scope.models.lists.OptionQuestion[i];
            if (data['question_text'].replace(/\s+/g, '') === question_text.replace(/\s+/g, '') && i != index)
                questionTextValid = false;
        });

        _.times($scope.models.lists.OptionQuestionTrigger['length'] , function(i){
            var data = $scope.models.lists.OptionQuestionTrigger[i];
            if (data['question_text'].replace(/\s+/g, '') === question_text.replace(/\s+/g, ''))
                questionTextValid = false;
        });

        return questionTextValid;
    }

    function cancelItem(index) {
        if ($scope.models.lists.OptionQuestion[index].status != 'updating'){

            $ngBootbox.confirm({message:"¿Are you sure to cancel this question?",
                                title:'Cancel',
                            }).then(function() {
                                $scope.models.lists.OptionQuestion.splice(index, 1);
                            }, function() {});
        } else{
            $scope.listingOptionAnswerByOperation();
            if ($scope.changesOcurred){
                $ngBootbox.confirm({message:"Are there changes without saving, ¿are you sure to cancel this question?",
                                title:'Cancel',
                            }).then(function() {
                                $scope.originalData = findOriginal(index);
                                $scope.models.lists.OptionQuestion[index] = angular.copy($scope.originalData);
                            }, function() {});
            }else{
                $scope.originalData = findOriginal(index);
                $scope.models.lists.OptionQuestion[index] = angular.copy($scope.originalData);
            }

        }
    }

    function editItem(index) {
        if (!optionQuestionOpen()){
            $scope.originalData = angular.copy($scope.models.lists.OptionQuestion[index]);
            $scope.models.lists.Copies.push({referencia: 'index'+index, object : $scope.originalData});
            $scope.models.lists.OptionQuestion[index].status = 'updating';
            loadOptionAnswers($scope.models.lists.OptionQuestion[index]);
            loadAllOptionAnswer();
        }
    }
    
    function duplicateItem(index) {
        if (!optionQuestionOpen()){
            duplicateId = $scope.models.lists.OptionQuestion[index].id;
            duplicateLabel = $scope.models.lists.OptionQuestion[index].label;
            duplicateSurvey = $scope.models.lists.OptionQuestion[index].survey;
            duplicateType = $scope.models.lists.OptionQuestion[index].type;
            duplicateQuestionText = $scope.models.lists.OptionQuestion[index].question_text;
            duplicateOptionsAnswer = $scope.models.lists.OptionQuestion[index].optionsAnswer;
            $scope.models.lists.OptionQuestion.splice(index+1, 0, {id:duplicateId, label: duplicateLabel, survey: duplicateSurvey, type: duplicateType, question_text: duplicateQuestionText, options_answer: duplicateOptionsAnswer, status: 'duplicating'});
        }
        loadOptionAnswers($scope.models.lists.OptionQuestion[index+1]);
        loadAllOptionAnswer();
        loadOptionAnswersDuplicated($scope.models.lists.OptionQuestion[index+1]);
    }

    function removeItem(itemOptionQuestion, index) {
        $ngBootbox.confirm({message:"¿Are you sure to delete this question?",
                            title:'Remove',
                        }).then(function() {
                            $http.delete('/scory/api/survey/'+$scope.surveyCurrent+'/option_question/detail/'+itemOptionQuestion.id+'/', itemOptionQuestion)
                                .then(function successCallback(response) {
                                    $scope.models.lists.OptionQuestion.splice(index, 1);
                                    validSurveyId();
                                },function errorCallback(response) {
                                    showMessage(response);
                                }).catch(function error(response) {
                                    $scope.message = 'No se puede procesar su petición, intente más tarde.';
                                    openMessage();
                                });

                        }, function() {});
    }

    function listingOptionAnswerByOperation() {
        $scope.models.lists.OptionsAnswerSave = [];
        $scope.models.lists.OptionsAnswerDelete = [];
        $scope.changesOcurred = false;
        $scope.optionAnswerValid = true;
        $scope.optionQuestionValid = true;

        $scope.models.lists.OptionsAnswerSave = angular.copy($scope.models.lists.OptionsAnswerSaveDuplicate);

        //list of options_answer to save
        _.each($scope.models.lists.OptionAnswersNew, function (option_answer_current, index) {
            if (option_answer_current.operation === 'save'){
                name = option_answer_current.answerSelected;
                triggerQuestion = option_answer_current.triggerQuestionSelected;

                if (name){
                    urlImg = $scope.host+option_answer_current.urlImg;
                    if (option_answer_current.urlImg === '/static/scory_web_frontend/img/emoji.png')
                        urlImg = '';
                    $scope.changesOcurred = true;
                    $scope.models.lists.OptionsAnswerSave.push({name: name, url_img: urlImg, trigger_question: triggerQuestion});
                }else{
                    $scope.optionAnswerValid = false;
                }

                if (triggerQuestion !== ''){
                    optionQuestionExist = _.find($scope.models.lists.OptionQuestionTrigger, {question_text: triggerQuestion});
                    if (!optionQuestionExist)
                        $scope.optionQuestionValid = false;
                }
            }
        });
        
        //list of options_answer to delete
        _.each($scope.models.lists.OriginalOptionAnswers, function (option_answer_original, index) {
            if (option_answer_original.operation === 'delete'){
                $scope.changesOcurred = true;
                $scope.models.lists.OptionsAnswerDelete.push(option_answer_original);
            }
        });
    }
    
    function verifyExistenceOptionAnswer() {
        $scope.models.lists.NameOptionAnswers = [];
        existence = false;

        _.each($scope.models.lists.OptionAnswers, function (option_answer_save, index) {
            name = option_answer_save.name.toLowerCase();

            if (option_answer_save.triggerQuestion)
                name = name.replace(/\s/g, "")+option_answer_save.triggerQuestion.question_text;
            else
                name = name.replace(/\s/g, "")

            if (!$scope.models.lists.NameOptionAnswers.includes(name))
                $scope.models.lists.NameOptionAnswers.push(name);
            else
                existence = true;
        });

        _.each($scope.models.lists.OptionsAnswerSave, function (option_answer_to_save, index) {
            name = option_answer_to_save.name.toLowerCase();
             name = name.replace(/\s/g, "")+option_answer_to_save.trigger_question;
            if (!$scope.models.lists.NameOptionAnswers.includes(name))
                $scope.models.lists.NameOptionAnswers.push(name);
            else if (option_answer_to_save.operation !== 'duplicating')
                existence = true;
        });

        return existence;
    }

    function verifyOptionAnswerWithoutPreviousSaved() {
        $scope.models.lists.NameOptionAnswers = [];
        existencePreviousSaved = false;
        _.each($scope.models.lists.OriginalOptionAnswers, function (option_answer_save, index) {
            name = option_answer_save.name.toLowerCase();
            if (option_answer_save.triggerQuestion)
                name = name.replace(/\s/g, "") + option_answer_save.triggerQuestion.question_text;
            else
                name = name.replace(/\s/g, "")

            if (!$scope.models.lists.NameOptionAnswers.includes(name))
                $scope.models.lists.NameOptionAnswers.push(name);
            else{
                existencePreviousSaved = true;
            }
        });

        _.each($scope.models.lists.OptionsAnswerSave, function (option_answer_to_save, index) {
            name = option_answer_to_save.name.toLowerCase();
            name = name.replace(/\s/g, "") + option_answer_to_save.trigger_question;
            if (!$scope.models.lists.NameOptionAnswers.includes(name))
                $scope.models.lists.NameOptionAnswers.push(name);
            else if (option_answer_to_save.operation !== 'duplicating')
                existencePreviousSaved = true;
        });
        return existencePreviousSaved;
    }

    function contOptionAnswerByOptionQuestion(option_question) {
        count = 0;
        count += $scope.models.lists.OptionsAnswerSave.length;
        if (option_question.status !== 'duplicating'){
            count += $scope.models.lists.OptionAnswers.length;
        }
        return count;
    }

    function saveOptionQuestion(itemOptionQuestion, pos) {
        if (itemOptionQuestion.type.name === 'multi_option'){
            listingOptionAnswerByOperation();

            if (!$scope.optionAnswerValid){
                showAlert("alert-danger","Verifique el campo de opción.", pos);
                return;
            }

            if (!$scope.optionQuestionValid){
                showAlert("alert-danger","Verifique el salto de pregunta.", pos);
                return;
            }

            if (verifyExistenceOptionAnswer()){
                showAlert("alert-danger","Existen opciones de repuestas duplicadas, para esta pregunta", pos);
                return;
            }

            if (verifyOptionAnswerWithoutPreviousSaved()){
                showAlert("alert-danger","Está opción de respuesta acaba de ser eliminada, si desea mantener la misma opción presione el boton cancelar.", pos);
                return;
            }

            if (contOptionAnswerByOptionQuestion(itemOptionQuestion) < 2){
                showAlert("alert-danger","Las preguntas de opción múltiple deben contener al menos dos opciones de respuesta.", pos);
                return;
            }

            itemOptionQuestion.listOptionsAnswerSave = $scope.models.lists.OptionsAnswerSave;
            itemOptionQuestion.listOptionsAnswerDelete = $scope.models.lists.OptionsAnswerDelete;
        }

        if (itemOptionQuestion.question_text === '' || itemOptionQuestion.question_text === null){
            showAlert("alert-danger","El texto es requerido.", pos);
        }else if(!validQuestionText(itemOptionQuestion.question_text, pos)){
            showAlert("alert-danger","Ya existe una pregunta o salto de pregunta con el mismo texto", pos);
        }else if(!itemOptionQuestion.hasOwnProperty("id") || itemOptionQuestion.status === 'duplicating'){
            //save
            itemOptionQuestion.sequence = pos+1;
            itemOptionQuestion.mandatory = 0;
            $http.post('/scory/api/survey/'+$scope.surveyCurrent+'/option_question/', itemOptionQuestion)
                .then(function successCallback(response) {
                    $scope.models.lists.OptionQuestion[pos] = response.data.option_question;
                    $scope.models.lists.OptionQuestion[pos].status = 'saved';

                    angular.forEach($scope.models.lists.OptionQuestion, function (option_question, index) {
                        if (response.data.dict[option_question.id]){
                            option_question.sequence = response.data.dict[option_question.id];
                        }
                    });
                    showAlert("alert-success","Pregunta guardada exitosamente.", pos);
                    load();
                },function errorCallback(response) {
                    showMessage(response);
                }).catch(function error(response) {
                    showAlert("alert-danger","No se puede procesar su petición, intente más tarde.", pos);
                });
        }
        else{
            $scope.originalData = findOriginal(pos);
            if (itemOptionQuestion.question_text === $scope.originalData.question_text && !$scope.changesOcurred){
                showAlert("alert-danger","No existen cambios para está pregunta.", pos);
                itemOptionQuestion.status = 'saved';
            }else{
                //update
                $http.put('/scory/api/survey/'+$scope.surveyCurrent+'/option_question/detail/'+itemOptionQuestion.id+'/', itemOptionQuestion)
                        .then(function successCallback(response) {
                            $scope.models.lists.OptionQuestion[pos].status = 'saved';
                            showAlert("alert-success","Pregunta actualizada exitosamente.", pos);
                            load();
                        },function errorCallback(response) {
                            showMessage(response);
                        }).catch(function error(response) {
                            $scope.message = 'No se puede procesar su petición, intente más tarde.';
                            openMessage();
                        });
            }
        }

    }

    function onDragEnd(event, dropEffect, item) {

        if (!cancelUpdateSequenceList()){
            if (item.sequence > $scope.positionDragEnd){
                $scope.positionDragEnd = $scope.positionDragEnd+1;
            }
            item.new_sequence = $scope.positionDragEnd;
            if (item.sequence !== item.new_sequence){
                $http.put('/scory/api/survey/'+$scope.surveyCurrent+'/option_question/'+item.id+'/', item)
                        .then(function successCallback(response) {
                            angular.forEach($scope.models.lists.OptionQuestion, function (option_question, index) {
                                if (response.data[option_question.id]){
                                    console.log(option_question.question_text+' viene de '+option_question.sequence+' va para '+response.data[option_question.id]);
                                    option_question.sequence = response.data[option_question.id];
                                }
                            });
                            validSurveyId();

                        }).catch(function error(response) {
                            $scope.message = 'No se puede procesar su petición, intente más tarde.';
                            openMessage();
                        });
            }
        }
    }

    function logListEvent(action, index, external, type) {
        $scope.positionDragEnd = index;
        loadAllOptionAnswer();
        loadInitialOptionAnswers();
    }

    function optionQuestionOpen() {
        var contOptionQuestionOpen = 0;
        var positionOpen = null;
        angular.forEach($scope.models.lists.OptionQuestion, function (option_question, index) {
            if (option_question.status !== 'saved'){
                contOptionQuestionOpen++;
            }
        });

        if (contOptionQuestionOpen > 0){
            $scope.message = 'Verifique que todas las preguntas esten guardadas.';
            openMessage();
            return true;
        }

        return false;
    }

    function cancelUpdateSequenceList() {
        var contOptionQuestionOpen = 0;
        _.each($scope.models.lists.OptionQuestion, function (option_question, index) {
            if (option_question.status !== 'saved'){
                contOptionQuestionOpen++;
            }
        });

        if (contOptionQuestionOpen > 0){
            return true;
        }

        return false;
    }

    function loadInitialOptionAnswers() {
        $scope.models.lists.OptionAnswersNew.push({urlImg: '/static/scory_web_frontend/img/emoji.png', answerSelected:'', triggerQuestionSelected: "", operation: 'save'});
        $scope.models.lists.OptionAnswersNew.push({urlImg: '/static/scory_web_frontend/img/emoji.png', answerSelected:'', triggerQuestionSelected: "", operation: 'save'});
    }

    function addOptionAnswer() {
        $scope.models.lists.OptionAnswersNew.push({urlImg: '/static/scory_web_frontend/img/emoji.png', answerSelected:'', triggerQuestionSelected: "", operation: 'save'});
    }

    function deleteOptionAnswer(index) {
        $ngBootbox.confirm({message:"¿Are you sure to delete this option answer?",
                            title:'Remove',
                        }).then(function() {
                            optionAnswerRemoved = angular.copy($scope.models.lists.OptionAnswers[index]);
                            $scope.models.lists.OptionAnswers.splice(index, 1);
                            if (optionAnswerRemoved.operation !== 'save'){
                                _.each($scope.models.lists.OriginalOptionAnswers, function(option_answer_original, index){
                                    if (option_answer_original.id === optionAnswerRemoved.id)
                                        option_answer_original.operation = 'delete';
                                })
                            }

                            _.each($scope.models.lists.OptionsAnswerSaveDuplicate, function(option_answer_save, index2){
                                if (option_answer_save.id === optionAnswerRemoved.id)
                                    $scope.models.lists.OptionsAnswerSaveDuplicate.splice(index, 1);
                            })
                        }, function() {});
    }

    function deleteOptionAnswerNew(index) {
        $ngBootbox.confirm({message:"¿Are you sure to delete this option answer new?",
                            title:'Remove',
                        }).then(function() {
                            $scope.models.lists.OptionAnswersNew.splice(index, 1);
                        }, function() {});
        }

    /***************************************************************************/
    /*******************MANIPULATING BUTTONS PUBLISH AND SHARE *****************/

    function changeStatus() {
        $ngBootbox.confirm({message:"Al publicar la encuesta, no podrá continuar con su edición, ¿está seguro?",
                            title:'Publicar',
                        }).then(function() {
                            var survey = $scope.survey;
                            survey.newStatus = 'Desactivada';
                            if (survey.status === 'Desactivada')
                                survey.newStatus = 'Activada';
                            $http.put('/scory/api/survey/'+survey.pk+'/change_status/', survey)
                                .then(function successCallback(response) {
                                    $scope.survey.status = survey.newStatus;
                                    $scope.disabledShare = true;
                                },function errorCallback(response) {
                                    showMessage(response);
                                }).catch(function error(response) {
                                    showAlert("alert-danger",'No se puede procesar su petición, intente más tarde.');
                                });
                        }, function() {});
    }

    function openMessenger(survey){
        $window.open(survey.url_messenger);
    }

    function downloadCodeMessenger(survey){
        var urlCodeMessenger = survey.url_messenger_code;
        var a  = document.createElement('a');
        a.href = urlCodeMessenger;
        a.download = 'imageCodeMessenger.png';
        a.click();
    }

    function openModalFormShare(survey) {
        $scope.surveyShare = survey;
        if (!survey.url_messenger) {
            id = survey.id ? survey.id : survey.pk;
            $http.put('/scory/api/survey/'+id+'/generate_url/', survey)
                .then(function successCallback(response) {
                    $scope.surveyShare.url_messenger = response.data.url_messenger;
                },function errorCallback(response) {
                    $scope.showMessage(response);
                }).catch(function error(response) {
                     scope.showAlert("alert-danger",'No se puede procesar su petición, intente más tarde.');
                });
        }
        $scope.modalForm = $uibModal.open({
          templateUrl: 'modalFormShare.html',
          controller: 'ModalInstanceSurvey',
          scope: $scope,
        });
    }

    /***************************************************************************/
    /*************************************MENSAJES******************************/
    function showMessage(response) {
        if (angular.isString(response.data))
            $scope.message = response.data;
        else if(angular.isObject(response.data)){
            $scope.message = "";
            _.each(response.data, function(arraysMessages, keyError){
                //TODO verificar si siempre sera un arreglo de mensajes de error
                _.each(arraysMessages, function(message, index){
                    $scope.message += message;
                    if(arraysMessages.length > index+1)
                         $scope.message += ", "
                })
            })
        }
        openMessage();
    }

    function openMessage() {
        $scope.modalMessage =$uibModal.open({
          templateUrl: 'messageSurvey.html',
          controller: 'ModalInstanceSurvey',
          scope: $scope,
        });
    }

    function openModalForm() {
        $scope.modalForm = $uibModal.open({
          templateUrl: 'modalFormSurvey.html',
          controller: 'ModalInstanceSurvey',
          scope: $scope,
        });
    }

    function dismissAlert() {
        $scope.alert = false;
        $scope.alertMessageStart = false;
        $scope.alertMessageEnd = false;
    }

    function showAlert(type,message, pos) {
        if (pos === null)
            $scope.alertSurvey = true;
        else if (pos === 'start')
            $scope.alertMessageStart = true;
        else if (pos === 'end')
            $scope.alertMessageEnd = true;
        else
            $scope.alert = true;
        $scope.type = type;
        $scope.messageAlert = message;
        $scope.position = pos;
        $timeout(function() {
            $scope.alert = false;
            $scope.alertSurvey = false;
            $scope.alertMessageStart = false;
            $scope.alertMessageEnd = false;
        }, 5000);
    }
    
    /***************************************************************************/
    /***************************MESSAGE START-END******************************/
    function cancelMessage(type){
        if(type === 'end'){
            $scope.editMessageEnd = false;
            $scope.survey.end_message = angular.copy($scope.originalMessageEnd);
        }else {
            $scope.editMessageStart = false;
            $scope.survey.start_message = angular.copy($scope.originalMessageStart);
        }
    }

    function editMessage(type){
        if(type === 'end'){
            $scope.editMessageEnd = true;
            $scope.originalMessageEnd = angular.copy($scope.survey.end_message);
        }else {
            $scope.editMessageStart = true;
            $scope.originalMessageStart = angular.copy($scope.survey.start_message);
        }
    }

    function saveMessage(survey,typeMessage) {
        pos = $scope.models.lists.OptionQuestion.length - 1;
        if (typeMessage === 'start' && survey.start_message === $scope.originalMessageStart){
            showAlert("alert-danger",'No existen cambios para el mensaje.', 'start');
            $scope.editMessageStart = false;
        }else if (typeMessage === 'end' && survey.end_message === $scope.originalMessageEnd){
            showAlert("alert-danger",'No existen cambios para el mensaje.', 'end');
            $scope.editMessageEnd = false;
        }else{
            $http.put('/scory/api/survey/'+$scope.surveyCurrent+'/update_messages/', survey)
                .then(function successCallback(response) {
                    if (typeMessage === 'start'){
                        $scope.editMessageStart = false;
                        showAlert("alert-success",'El mensaje ha sido actualizado exitosamente.', 'start');
                    }else if (typeMessage === 'end'){
                        $scope.editMessageEnd = false;
                        showAlert("alert-success",'El mensaje ha sido actualizado exitosamente.', 'end');
                    }
                },function errorCallback(response) {
                    showMessage(response);
                }).catch(function error(response) {
                    showAlert("alert-danger",'No se puede procesar su petición, intente más tarde.');
                });
        }
    }

    /***************************************************************************/
    /***************************JUMPERS QUESTIONS+******************************/

    function jumpers() {
        loadOptionsTrigger();
    }

    function loadOriginTrigger() {
        _.each($scope.models.lists.OptionQuestionTrigger, function (option_question_trigger, index) {
            $scope.models.lists.OriginTrigger = [];
            _.each($scope.models.lists.OptionQuestion, function (option_question, indexOQ) {
                if (option_question.type.name == 'multi_option'){
                    _.each(option_question.optionsAnswer, function (optionAnswer, indexOA) {
                        if (option_question_trigger.id === optionAnswer.triggerQuestion.id) {
                            if (!$scope.models.lists.OriginTrigger.includes(indexOQ+1)){
                                $scope.models.lists.OriginTrigger.push(indexOQ+1);
                            }
                        }
                    });
                }
            });
            $scope.models.lists.OptionQuestionTrigger[index].origins = $scope.models.lists.OriginTrigger;
        });
    }

    // Generate initial model OptionQuestionTrigger
    function loadOptionsTrigger() {
        $scope.models.lists.OptionQuestionTrigger = [];
        $http.get('/scory/api/survey/'+$scope.surveyCurrent+'/option_question_sequence_null/')
            .then(function success(response) {
                _.each(response.data, function (value, index) {
                    value.status = 'saved';
                    $scope.models.lists.OptionQuestionTrigger.push(value);
                });
                loadOptionQuestion();
            }).catch(function error(response) {
                    alert('error');
            });
    }

    function cancelItemTrigger(index) {
        if ($scope.models.lists.OptionQuestionTrigger[index].status !== 'updating'){

            $ngBootbox.confirm({message:"¿Are you sure to cancel this question?",
                                title:'Cancel',
                            }).then(function() {
                                $scope.models.lists.OptionQuestionTrigger.splice(index, 1);
                            }, function() {});
        } else{
            if ($scope.changesOcurred){
                $ngBootbox.confirm({message:"Are there changes without saving, ¿are you sure to cancel this question?",
                                title:'Cancel',
                            }).then(function() {
                                $scope.originalData = findOriginal(index);
                                $scope.models.lists.OptionQuestionTrigger[index] = angular.copy($scope.originalData);
                            }, function() {});
            }else{
                $scope.originalData = findOriginal(index);
                $scope.models.lists.OptionQuestionTrigger[index] = angular.copy($scope.originalData);
            }

        }
    }

    function editItemTrigger(index) {
        if (!optionQuestionOpen()){
            $scope.originalData = angular.copy($scope.models.lists.OptionQuestionTrigger[index]);
            $scope.models.lists.Copies.push({referencia: 'index'+index, object : $scope.originalData});
            $scope.models.lists.OptionQuestionTrigger[index].status = 'updating';
        }
    }

    function removeItemTrigger(itemOptionQuestionTrigger, index) {
        console.log(itemOptionQuestionTrigger);
        if (itemOptionQuestionTrigger.origins.length > 0) {
            messageDelete = "Salto de pregunta en uso para la(s) pregunta(s) N°" + 
                            itemOptionQuestionTrigger.origins.join(', N°') +
                            " ¿está seguro de eliminarlo?";
        } else {
            messageDelete = "¿Está seguro de eliminar este salto de pregunta?";
        }
        $ngBootbox.confirm({message:messageDelete,
                            title:'Eliminar',
                        }).then(function() {
                            $http.delete('/scory/api/survey/'+$scope.surveyCurrent+'/option_question_trigger/detail/'+itemOptionQuestionTrigger.id+'/', itemOptionQuestionTrigger)
                                .then(function successCallback(response) {
                                    $scope.models.lists.OptionQuestionTrigger.splice(index, 1);
                                    validSurveyId();
                                },function errorCallback(response) {
                                    showMessage(response);
                                }).catch(function error(response) {
                                    $scope.message = 'No se puede procesar su petición, intente más tarde.';
                                    openMessage();
                                });

                        }, function() {});
    }

    function validQuestionTriggerText(questionTriggerText, index){
        var questionTextValid = true;
        _.times($scope.models.lists.OptionQuestion['length'], function(i){
            var data = $scope.models.lists.OptionQuestion[i];
            if (data['question_text'].replace(/\s+/g, '') === questionTriggerText.replace(/\s+/g, ''))
                questionTextValid = false;
        });

        _.times($scope.models.lists.OptionQuestionTrigger['length'] , function(i){
            var data = $scope.models.lists.OptionQuestionTrigger[i];
            if (data['question_text'].replace(/\s+/g, '') === questionTriggerText.replace(/\s+/g, '') && i != index)
                questionTextValid = false;
        });
        return questionTextValid;
    }

    function saveOptionQuestionTrigger(itemTrigger,pos) {

        if (itemTrigger.question_text == '' || itemTrigger.question_text === null){
            showAlert("alert-danger","El texto es requerido.", pos);
        }else if(!validQuestionTriggerText(itemTrigger.question_text, pos)){
            showAlert("alert-danger","Ya existe una pregunta o salto de pregunta con el mismo texto", pos);
        }else if(!itemTrigger.hasOwnProperty("id") || itemTrigger.status === 'duplicating'){
        //save
        $http.post('/scory/api/survey/'+$scope.surveyCurrent+'/option_question_trigger/', itemTrigger)
            .then(function successCallback(response) {
                $scope.models.lists.OptionQuestionTrigger[pos] = response.data.option_question;
                $scope.models.lists.OptionQuestionTrigger[pos].status = 'saved';
                showAlert("alert-success","Pregunta guardada exitosamente.", pos);
                load();
            },function errorCallback(response) {
                showMessage(response);
            }).catch(function error(response) {
                showAlert("alert-danger","No se puede procesar su petición, intente más tarde.", pos);
            });
        }
        else{
            $scope.originalData = findOriginal(pos);
            if (itemTrigger.question_text === $scope.originalData.question_text && !$scope.changesOcurred){
                showAlert("alert-danger","No existen cambios para está pregunta.", pos);
                itemTrigger.status = 'saved';
            }else{
                //update
                $http.put('/scory/api/survey/'+$scope.surveyCurrent+'/option_question/detail/'+itemTrigger.id+'/', itemTrigger)
                        .then(function successCallback(response) {
                            $scope.models.lists.OptionQuestionTrigger[pos].status = 'saved';
                            showAlert("alert-success","Pregunta actualizada exitosamente.", pos);
                            load();
                        },function errorCallback(response) {
                            showMessage(response);
                        }).catch(function error(response) {
                            $scope.message = 'No se puede procesar su petición, intente más tarde.';
                            openMessage();
                        });
            }
        }

    }
});
