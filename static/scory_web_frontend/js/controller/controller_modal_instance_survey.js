app.controller('ModalInstanceSurvey', function ($scope, $uibModalInstance, $window) {
      $scope.titleMessage = "Información";
      $scope.bodyMessage = $scope.message;

      $scope.closeMessage = function () {
        $uibModalInstance.close();
        if ($scope.modalClose == true){
            $scope.closeModal();
            $window.location.href = "#!/survey_edit";
        }
      };


      $scope.closeModal = function () {
        $scope.formSurvey = {};
        $scope.formSurvey.$dirty;
        $scope.modalForm.close();
      };
});