app.controller('ConfigSurveyController', function($scope, $ngBootbox, $http, $uibModal, $routeParams, $timeout) {

    $scope.models = {
        lists: { "Companies":[]}
    };

    $scope.load = function() {
        $scope.showButtonDownload = false;
        $scope.surveyCurrent = $routeParams.survey;
        $scope.loadInfoSurvey();        
    };

    $scope.loadInfoSurvey = function() {
        $scope.models.lists.Companies = [];
        $http.get('/scory/api/survey/detail/'+$scope.surveyCurrent)
            .then(function success(response) {
                angular.forEach(response.data, function (survey, index) {
                    $scope.survey = survey;
                    if ($scope.survey.url_messenger_code)
                        $scope.showButtonDownload = true;
                });
                $scope.loadCompanies();
            }).catch(function error(response) {
                    alert('error');
            });
    }

    // Generate initial lists Companies
    $scope.loadCompanies = function(){
        $scope.models.lists.Companies = [];
        $http.get('/scory/api/company/')
            .then(function success(response) {
                angular.forEach(response.data, function (company, index) {
                    $scope.models.lists.Companies.push({id: company.pk, name: company.name});
                });
                $scope.selectedOption = {id: $scope.survey.company.pk};
            }).catch(function error(response) {
                    alert('error');
            });
    };

    $scope.submit = function(survey){
        if (survey.name == undefined){
            $scope.showAlert("alert-danger",'Indique el nombre de la encuesta.');
            return;
        }
        if ($scope.selectedOption == undefined){
            $scope.showAlert("alert-danger",'Seleccione una compañia');
            return;
        }
        if (survey.required_time_expired){
            if (survey.time_expired_minutes == undefined){
                $scope.showAlert("alert-danger",'Indique el tiempo de expiración en minutos (superior o igual a un minuto)');
                return;
            }
        }
        survey.company = $scope.selectedOption;
        $http.put('/scory/api/survey/detail/'+$scope.surveyCurrent+'/', survey)
            .then(function successCallback(response) {
                $scope.showAlert("alert-success",'La encuesta ha sido configurada exitosamente.');
            },function errorCallback(response) {
                $scope.showMessage(response);
            }).catch(function error(response) {
                 scope.showAlert("alert-danger",'No se puede procesar su petición, intente más tarde.');
            });
    };

     $scope.showMessage = function (response){
        if (angular.isString(response.data))
            $scope.message = response.data;
        else if(angular.isObject(response.data)){
            $scope.message = "";
            angular.forEach(response.data, function(arraysMessages, keyError){
                //TODO verificar si siempre sera un arreglo de mensajes de error
                angular.forEach(arraysMessages, function(message, index){
                    $scope.message += message;
                    if(arraysMessages.length > index+1)
                         $scope.message += ", "
                })
            })
        }
        $scope.openMessage();
    }

    $scope.openMessage = function () {
        $scope.modalMessage =$uibModal.open({
          templateUrl: 'messageSurvey.html',
          controller: 'ModalInstanceSurvey',
          scope: $scope,
        });
    };

    $scope.dismissAlert= function(){
        $scope.alert = false;
    }

    $scope.showAlert = function (type,message) {
        $scope.alert = true;
        $scope.type = type;
        $scope.messageAlert = message;
        $timeout(function() {
            $scope.alert = false;
        }, 2000);
    };

});