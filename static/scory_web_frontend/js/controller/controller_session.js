appAcount.controller('SessionController', function($scope, $route, $ngBootbox, $http, $uibModal, $window, $timeout, $location) {
    $scope.submitSession = function(user){
        $http.post('/scory/api/session/',user)
            .then(function success(response) {
                if  (response.data.status == 200){
                    $window.location.href = "/scory/user_index";
                }else {
                    $scope.message = response.data.message;
                    $scope.openMessage();
                }
            }).catch(function error(response) {
                    $scope.message = 'No se puede procesar su petición, intente más tarde.';
                    $scope.openMessage();
            });
    };

    $scope.openMessage = function () {
        $scope.modalMessage =$uibModal.open({
          templateUrl: 'message.html',
          controller: 'ModalInstance',
          scope: $scope,
        });
    };
});
