app.controller('FormSurveyController', function($scope, $ngBootbox, $http, $uibModal, $timeout, $window, Service) {

    $scope.models = {
        selected: null,
        lists: { "Companies":[]}
    };

    $scope.survey = {};
    $scope.progressOn = false;

    // Generate initial lists Companies
    $scope.loadCompanies = function(){
        $scope.models.lists.Companies = [];
        $http.get('/scory/api/company/')
            .then(function success(response) {
                for (var i = 0; i < response.data['length']; i++) {
                    var company = response.data[i];
                    $scope.models.lists.Companies.push({id: company['pk'], name: company['name']});
                }
            }).catch(function error(response) {
                    alert('error');
            });
    };
    $scope.loadCompanies();


    $scope.submit = function(survey){
        if (!survey){
            $scope.message = 'Complete los campos requeridos';
            $scope.openMessage();
        }else {
            if (survey.company == undefined){
                $scope.message = 'Verifique la información en company';
                $scope.openMessage();
                return;
            }

            if (survey.required_time_expired){
                if (survey.time_expired_minutes == undefined){
                    $scope.message = 'Verifique la información del tiempo de expiración';
                    $scope.openMessage();
                    return;
                }
            }

            survey.company = survey.company.id
            $scope.modalClose = false;
            startProgress();
            $scope.progressOn = true;
            $http.post('/scory/api/survey/',survey)
                .then(function success(response) {
                    
                    if  (response.data.status == 201){
                        $scope.formSurvey = {};
                        $scope.formSurvey.$dirty;
                        $scope.modalForm.close();
                        Service.data.alert = true;
                        $window.location.href = "#!/survey_edit/"+response.data.survey.pk;
                    }else {
                        wrongFields = '';
                        if (response.data.error.name)
                            wrongFields += 'nombre, ';
                        if (response.data.error.company)
                            wrongFields += 'company, ';
                        if (response.data.error.required_time_expired)
                            wrongFields += 'required time expired';
                        if (response.data.error.time_expired_minutes)
                            wrongFields += 'time expired minutes';

                        $scope.message = 'Verifique la información introducida (' + wrongFields + ').';
                        $scope.openMessage();
                        $scope.progressOn = false;
                    }
                }).catch(function error(response) {
                        $scope.progressOn = false;
                        $scope.message = 'No se puede procesar su petición, intente más tarde.';
                        $scope.openMessage();
                });
        }
    };

    $scope.openMessage = function () {
        $scope.modalMessage =$uibModal.open({
          templateUrl: 'messageSurvey.html',
          controller: 'ModalInstanceFormSurvey',
          scope: $scope,
        });
    };

    $scope.openModalForm = function () {
        $scope.modalForm = $uibModal.open({
          templateUrl: 'modalFormSurvey.html',
          controller: 'ModalInstanceFormSurvey',
          scope: $scope,
        });
    };

    /*PROGRESS CIRCULAR*/
    $scope.progressCircleData = {
        value: 0
    };
    $scope.timeInMs = 0;
        
    var newvalue = 0, totalWaitInMin = 1, totalWait = totalWaitInMin * 60 * 1000, increment = 500, i = 0,
    updateProgress = function() {
      $scope.timeInMs += increment;
      newvalue += increment;
      $scope.progressCircleData = {
        value: Math.floor(newvalue / (increment * 0.05))
      };
    },
    startProgress = function() {
      if (newvalue < totalWait) {
        promise = $timeout(function() {
          updateProgress();
          startProgress();
        }, increment);
      } else {
        $timeout.cancel(promise);
      }
    };
});