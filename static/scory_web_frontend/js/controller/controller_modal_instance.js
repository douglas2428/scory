appAcount.controller('ModalInstance', function ($scope, $uibModalInstance) {
    $scope.titleMessage = "Información";
    $scope.bodyMessage = $scope.message;
    $scope.closeMessage = function () {
       $uibModalInstance.close();
       if ($scope.modalClose == true)
           $scope.closeModal();
    };
});