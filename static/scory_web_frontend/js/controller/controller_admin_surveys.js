app.controller('AdminSurveysController', function($scope, $ngBootbox, $http, $uibModal, $window) {

    $scope.models = {
        selected: null,
        lists: {"OptionQuestion":[], "OptionAnswersAll":[]}
    };

    // Generate initial lists Question by Survey
    $scope.countQuestionsBySurvey = function(surveyId, indexSurvey){
        $scope.modelsQuestions = [];
        $scope.count = 0;
        $http.get('/scory/api/survey/'+surveyId+'/question/')
            .then(function success(resp) {
                $scope.modelsSurveys[indexSurvey].count = resp.data.length;
            }).catch(function error(resp) {
                    alert('error');
            });
    };

    // Generate initial lists Surveys
    $scope.loadSurveys = function(){
        $scope.modelsSurveys = [];
        $http.get('/scory/api/survey/')
            .then(function success(response) {
                for (var i = 0; i < response.data['length']; i++) {
                    var survey = response.data[i];
                    $scope.countQuestionsBySurvey(survey['pk'], i);
                    $scope.modelsSurveys.push({id: survey['pk'], name: survey['name'], status: survey['status'], company: survey['company'], url_messenger: survey['url_messenger'], url_messenger_code: survey['url_messenger_code'], count: $scope.count});
                }
            }).catch(function error(response) {
                    alert('error');
            });
    };
    $scope.loadSurveys();
    

    $scope.openSurvey = function (survey) {
        if (survey.status == 'Desactivada') 
            $window.location.href = "#!/survey_edit/"+survey.id;
        else{
            $scope.message = 'La encuesta se encuentra activada, desactivela y proceda a editarla.'
            $scope.openMessageAdminSurvey();
        }
    };

    $scope.configSurvey = function (survey) { 
        $window.location.href = "#!/survey_config/"+survey;
    };

    $scope.editSurvey = function (survey) { 
        $window.location.href = "#!/survey_edit/"+survey;
    };

    $scope.deleteSurvey = function(survey, index){
        $http.delete('/scory/api/survey/detail/'+survey.id+'/')
            .then(function successCallback(response) {
                $scope.modalMessage.close();
                if (response.data.status == 400){
                    $scope.showAlert("alert-danger",response.data.error+' ('+survey.name+').', null);
                }else{
                    $scope.modelsSurveys.splice(index, 1);
                    $scope.showAlert("alert-success","Encuesta ("+survey.name+") eliminada exitosamente", null);
                }
            },function errorCallback(response) {
                $scope.showMessage(response);
            }).catch(function error(response) {
                $scope.message = 'No se puede procesar su petición, intente más tarde.';
                $scope.openMessage();
            });
    };

    $scope.changeStatus = function(survey, index){
        survey.newStatus = 'Desactivada';
        if (survey.status == 'Desactivada'){
            survey.newStatus = 'Activada';
        }
        $http.put('/scory/api/survey/'+survey.id+'/change_status/', survey)
            .then(function successCallback(response) {
                $scope.modelsSurveys[index].status = survey.newStatus;
            },function errorCallback(response) {
                $scope.showMessage(response);
            }).catch(function error(response) {
                 scope.showAlert("alert-danger",'No se puede procesar su petición, intente más tarde.');
            });
    }

    $scope.openMessageDeleteSurvey = function (survey, index) {
        $scope.message = "¡Atención! si elimina la encuesta se borrarán todos los datos y esto no se puede deshacer.";
        $scope.survey = survey;
        $scope.index = index;
        $scope.modalMessage =$uibModal.open({
          templateUrl: 'messageDeleteSurvey.html',
          controller: 'ModalInstanceSurvey',
          scope: $scope,
        });
    }

    $scope.openMessageAdminSurvey = function () {
        $scope.modalMessage =$uibModal.open({
          templateUrl: 'messageSurvey.html',
          controller: 'ModalInstanceSurvey',
          scope: $scope,
        });
    };

    $scope.openModalPreviewSurvey = function (survey) {
        $scope.loadOptionQuestion(survey);
        $scope.surveyName = survey.name;
        $scope.modalForm = $uibModal.open({
          templateUrl: 'modalPreviewSurvey.html',
          controller: 'ModalInstanceSurvey',
          scope: $scope,
        });
    };

    $scope.loadOptionQuestion = function(survey){
        $scope.loadAllOptionAnswer();
        $scope.models.lists.OptionQuestion = [];
        $http.get('/scory/api/survey/'+survey.id+'/option_question/')
            .then(function success(response) {
                angular.forEach(response.data, function (value, index) {
                    $scope.models.lists.OptionAnswersByOptionQuestion = [];
                    if (value.options_answer){
                        angular.forEach($scope.models.lists.OptionAnswersAll, function (options_answer, index) {
                            angular.forEach(value.options_answer, function (id) {
                                if (id == options_answer.id){
                                    $scope.models.lists.OptionAnswersByOptionQuestion.push(options_answer);
                                }
                            });
                        });
                    }
                    value.optionsAnswer = $scope.models.lists.OptionAnswersByOptionQuestion;
                    value.status = 'saved';
                    $scope.models.lists.OptionQuestion.push(value);
                });
            }).catch(function error(response) {
                    alert('error');
            });
    };

    $scope.loadAllOptionAnswer = function (){
        $scope.models.lists.OptionAnswersAll = [];
        $http.get('/scory/api/options_answer/')
            .then(function success(response) {
                angular.forEach(response.data, function (option_answer, index) {
                    $scope.models.lists.OptionAnswersAll.push({id: option_answer.pk, name: option_answer.name, urlImg: option_answer.url_img});
                });
            }).catch(function error(response) {
                    alert('error');
        });
    };


});