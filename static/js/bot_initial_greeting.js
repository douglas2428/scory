(function($){
    $(document).ready(function($){

        $('#id_token').attr('rows', '5');
        $('#id_initial_greeting').attr('rows', '5');
    	
    	$("#id_initial_greeting").attr("placeholder", "Type @user_facebook to refer to facebook user");
    	$( '<input type="button" id="id_button_delete_greeting" value="Delete Initial Greeting" style="width: 200px; margin: 35px">' ).insertAfter( "#id_initial_greeting" );
    	$( '<h5 id="id_label_placeholder" style="margin-left:25%">Type @user_facebook to refer to facebook user</>' ).insertAfter( "#id_button_delete_greeting" );


    	var initial_greeting = $('#id_initial_greeting').val();

        if ($('#id_initial_greeting').val() == "")
            $( "h5" ).remove( ":contains('Type @user_facebook to refer to facebook user')" );

    	function label_save_change(){
                            
            $( "h5" ).remove( ":contains('Type @user_facebook to refer to facebook user')" );
            if ($('#id_initial_greeting').val() != "")
                $( '<h5 id="id_label_placeholder" style="margin-left:25%">Type @user_facebook to refer to facebook user</>' ).insertAfter( "#id_button_delete_greeting" );	

            $( "h1" ).remove( ":contains('Press \"Save\" to save changes')" );
            if ($('#id_initial_greeting').val() != initial_greeting){
                $( '<h1 id="id_label_changed" style="margin-left:75%">Press \"Save\" to save changes</>' ).insertAfter( "#id_button_delete_greeting" );
            }
        }

        $( "#id_button_delete_greeting" ).click(function() {
		  	$('#id_initial_greeting').val('');
		  	label_save_change();

            $( "h1" ).remove( ":contains('Press \"Save\" to save changes')" );
            if ($('#id_initial_greeting').val() != initial_greeting){
                $( '<h1 id="id_label_changed" style="margin-left:75%">Press \"Save\" to save changes</>' ).insertAfter( "#id_button_delete_greeting" );
            }
		});

		$('#id_initial_greeting').on('input',function(e){
			label_save_change();
		}); 
    });
})(django.jQuery);
