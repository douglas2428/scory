var map, geocoder;
function cargarMapa() {
    var latlng = new google.maps.LatLng(-33.451760, -70.686131  );
    var mapOptions = {
        zoom: 13,
        center: latlng,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    map = new google.maps.Map(document.getElementById('mapa'), mapOptions);
    geocoder = new google.maps.Geocoder();

    google.maps.event.addListener(map, 'click', function(event) {
        geocoder.geocode({
            'latLng': event.latLng
            }, function(results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    if (results[0]) {
                      $(document).ready(function () {
                         $("#id_div_result_search").removeAttr("hidden");
                      });
                      document.getElementById('id_lat').innerHTML = results[0].geometry.location.lat().toFixed(6);
                      document.getElementById('id_lng').innerHTML = results[0].geometry.location.lng().toFixed(6);
                      map.setCenter(results[0].geometry.location);
                      document.getElementById('id_direccion').innerHTML = results[0].formatted_address;
                    }
                }
          });
    });
}

function codeAddress() {
    var address = document.getElementById('address').value;
    geocoder.geocode({
      'address': address
    }, function (results, status) {
      if (status == google.maps.GeocoderStatus.OK) {
          $(document).ready(function () {
              $("#id_div_result_search").removeAttr("hidden");
          });
          document.getElementById('id_lat').innerHTML = results[0].geometry.location.lat().toFixed(6);
          document.getElementById('id_lng').innerHTML = results[0].geometry.location.lng().toFixed(6);
          map.setCenter(results[0].geometry.location);
          document.getElementById('id_direccion').innerHTML = results[0].formatted_address;
          var marker = new google.maps.Marker({
              map: map,
              position: results[0].geometry.location
          });
          infowindow = new google.maps.InfoWindow({
              content: results[0].formatted_address + '<br> Latitud: ' + results[0].geometry.location.lat().toFixed(6) + '<br> Longitud: ' + results[0].geometry.location.lng().toFixed(6)
          });
          infowindow.open(map, marker)
      }else {
              $(document).ready(function () {
                  $("#id_div_result_search").attr("hidden", true);
              });
              alert('Geocode no tuvo éxito por la siguiente razón: ' + status)
            }
    })
};