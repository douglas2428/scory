(function($){
    $(document).ready(function($){
        
        /*OptionQuestionAdmin*/
        if ($("#id_type option:selected").html() != "multi_option")
        	$(".field-options_answer").hide();

        $("#id_type").click(function(){
            $(".field-options_answer").hide();
            if ($("#id_type option:selected").html() == "multi_option")
                $(".field-options_answer").show();
        });

        /*SurveyAdmin*/
        if($("#id_required_time_expired").is(':checked')) { 
            $('div[class="form-row field-time_expired_minutes"]').show(); 
        } else {  
            $('div[class="form-row field-time_expired_minutes"]').hide();  
        } 

        $("#id_required_time_expired").click(function() {  
	        if($("#id_required_time_expired").is(':checked')) { 
                $('div[class="form-row field-time_expired_minutes"]').show();
	        } else {  
                $('div[class="form-row field-time_expired_minutes"]').hide(); 
	        } 
        });
        
        /*Messenger Code*/
        $('div[class="form-row field-url_messenger_code"]').hide();

        /*Manipulating field visibility to generate messenger code*/
        var url = String(window.location);
        if (url.indexOf('change') == -1) {
            $('div[class="form-row field-image_messenger_code"]').hide();
        }  

        /*Creating button to generate messenger code and download image messenger code*/
        $( '<input type="button" id="id_button_generate_messenger_code" value="Generate Messenger Code" style="width: 200px; margin: 35px">' ).insertAfter( "#id_img_messenger_code" );
        $( '<input type="button" id="id_button_download_image" value="Download Image" style="width: 200px; margin: 35px">' ).insertAfter( "#id_button_generate_messenger_code" );

        if($("#id_url_messenger_code").val() == '') { 
            $("#id_button_download_image").hide();
        }

        /*Get survey_id from the URL*/
        $('div[class="form-row field-id"]').hide();
        var surveyId = $('div[class="form-row field-id"]').find( "p" ).text(); 

        /*Generating messenger code*/
        $("#id_button_generate_messenger_code").click(function() {
            var csrf = document.getElementsByName('csrfmiddlewaretoken')[0].value
            $.post("/bot/url_code_messenger/",{ survey_id: surveyId, csrfmiddlewaretoken: csrf },
                function(data) {
                    $('#id_url_messenger_code').val(data.url_code_messenger);
                    $('#id_img_messenger_code').attr('src',data.url_code_messenger);
                    $("#id_button_download_image").show(); 
                }
            );
        });

        /*Downloading image messenger code*/
        $("#id_button_download_image").click(function() {
            var urlCodeMessenger = $('#id_url_messenger_code').val();
            var a  = document.createElement('a');
            a.href = urlCodeMessenger;
            a.download = 'imageCodeMessenger.png';
            a.click();
        });

        /*Input end message survey*/
        $('#id_end_message').attr('rows', '3');
    });
})(django.jQuery);