(function($){
    $(document).ready(function($){
    	$( '<h1 id="id_label_ubication" style="margin-top:40px"> Ubication </>' ).insertAfter( "#id_identifier" );
    	$( '<input type="button" id="id_google_maps" value="Insert from Google Maps" style="width: 200px">' ).insertAfter( "#id_label_ubication" );
      	$('#id_latitude').attr('readonly', true);
      	$('#id_longitude').attr('readonly', true);

        $( "#id_google_maps" ).click(function() {
		  	var new_window = window.open('/bot/googleMaps','_blank','width=1200,height=600,resizable=0');
			new_window.onload = function() {
		  		new_window.cargarMapa();	

		  		new_window.document.getElementById("id_save_result").onclick = function () { 
		  			var lat = new_window.document.getElementById("id_lat").innerHTML;
		  			var lng = new_window.document.getElementById("id_lng").innerHTML;
		  			if (lat!=""){
		  				$("#id_latitude").val(lat);
		    			$("#id_longitude").val(lng);
		  			}
		    		new_window.close();
		  		};
			};
		});
    });
})(django.jQuery);
