var mainApp = angular.module('myapp', ['ui.bootstrap']);

mainApp.config(['$httpProvider', function($httpProvider) {
    $httpProvider.defaults.xsrfCookieName = 'csrftoken';
    $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';
}]);

mainApp.controller('ContactController', ['$scope', '$http', '$uibModal', '$window', function($scope, $http, $uibModal, $window) {

		$scope.redirect = function(){
			$window.location.href = "/scory/user_accounts";
		}

		$scope.submit = function(contact){
			$scope.modalClose = false;
			$http.post('/scory/api/contact/',contact)
				.then(function success(response) {
				    if  (response.data.status == 201){
				    	$scope.message = response.data.contact.name + ', gracias por contactarnos.';
				    	$scope.modalClose = true;
						$scope.openMessage();
					}else {
						wrongFields = '';
						if (response.data.error.name)
							wrongFields += 'nombre, ';
						if (response.data.error.mail)
							wrongFields += 'email, ';
						if (response.data.error.phone)
							wrongFields += 'telefono';

						$scope.message = 'Verifique la información introducida (' + wrongFields + ').';
						$scope.openMessage();
					}
				}).catch(function error(response) {
						$scope.message = 'No se puede procesar su petición, intente más tarde.';
						$scope.openMessage();
				});
		};

		$scope.openMessage = function () {
		    $scope.modalMessage =$uibModal.open({
		      templateUrl: 'message.html',
		      controller: 'ModalInstance',
		      scope: $scope,
		    });
		};

		$scope.openModalForm = function () {
		    $scope.modalForm = $uibModal.open({
		      templateUrl: 'modalForm.html',
		      controller: 'ModalInstance',
		      scope: $scope,
		    });
		};
}]);

mainApp.controller('ModalInstance', function ($scope, $uibModalInstance) {
	  $scope.titleMessage = "Información";
	  $scope.bodyMessage = $scope.message;

	  $scope.closeMessage = function () {
	    $uibModalInstance.close();
	    if ($scope.modalClose == true)
	  		$scope.closeModal();
	  };


	  $scope.closeModal = function () {
	  	$scope.formContact = {};
		$scope.formContact.$dirty;
	  	$scope.modalForm.close();
	  };

});