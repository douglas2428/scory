#!/bin/bash

PATH="/home/deploy/.nvm/versions/node/v8.12.0/bin:$PATH"
cd ~/develop_scory/dev_scory/current/my-app
npm install
ng build --configuration=develop