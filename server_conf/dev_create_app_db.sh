#!/bin/bash
# this script executes the sql code that creates the database and user used by the django app

script_path="`dirname \"$0\"`"
sudo -u postgres psql --file=$script_path/dev_createdb.sql