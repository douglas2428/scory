#!/bin/bash

PATH="/home/deploy/.nvm/versions/node/v8.12.0/bin:$PATH"
cd ~/production_scory/prod_scory/current/my-app
npm install
ng build --configuration=production