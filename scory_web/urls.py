from django.views.generic import TemplateView
from django.conf.urls import url
from scory_web import views
from django.contrib.auth.decorators import login_required
from django.contrib.auth.views import logout_then_login


urlpatterns = [
    url(r'^business', TemplateView.as_view(template_name="index.html"), name='index'),
    url(r'^api/session/$', views.SessionRestApi.as_view(), name='session'),
    url(r'^api/contact/$', views.ContactRestApi.as_view(), name='contact'),
    url(r'^user_index', login_required(TemplateView.as_view(template_name="user_index.html")), name='user_index'),
    url(r'^logout/', logout_then_login, name='logout'),
    url(r'^api/company/$', views.CompanyList.as_view(), name='company'),
    url(r'^api/survey/$', views.SurveyList.as_view(), name='survey_list'),
    url(r'^api/type/$', views.TypeList.as_view(), name='option_question_list'),
    url(r'^api/survey/(?P<survey_id>[\d]+)/option_question_sequence_null/$', views.OptionQuestionSequenceNullBySurveyList.as_view(), name='option_question_sequence_null_list'),
    url(r'^api/survey/(?P<survey_id>[\d]+)/option_question/$', views.OptionQuestionBySurveyList.as_view(), name='option_question_list'),
    url(r'^api/survey/(?P<survey_id>[\d]+)/option_question/detail/(?P<option_question_id>[\d]+)/$', views.OptionQuestionDetail.as_view(), name='option_question_detail'),
    url(r'^api/survey/(?P<survey_id>[\d]+)/option_question/(?P<option_question_id>[\d]+)/$', views.UpdateOptionQuestionAndSequence.as_view(), name='update_option_question_and_sequence'),
    url(r'^api/option_question/(?P<option_question_id>[\d]+)/options_answer/$', views.OptionAnswersByOptionQuestionList.as_view(), name='option_answers_by_option_question_list'),
    url(r'^api/options_answer/$', views.OptionAnswersList.as_view(), name='option_answers_list'),
    url(r'^api/survey/detail/(?P<survey_id>[\d]+)/$', views.SurveyDetail.as_view(), name='survey_detail'),
    url(r'^api/survey/(?P<survey_id>[\d]+)/change_status/$', views.SurveyStatus.as_view(), name='survey_status'),
    url(r'^api/survey/(?P<survey_id>[\d]+)/generate_url/$', views.SurveyUrl.as_view(), name='generate_url'),
    url(r'^api/survey/(?P<survey_id>[\d]+)/update_messages/$', views.SurveyMessages.as_view(), name='survey_messages'),
    url(r'^api/survey/(?P<survey_id>[\d]+)/option_question_trigger/$', views.OptionQuestionTriggerBySurveyList.as_view(), name='option_question_trigger'),
    url(r'^api/survey/(?P<survey_id>[\d]+)/option_question_trigger/detail/(?P<option_question_id>[\d]+)/$', views.OptionQuestionTriggerDetail.as_view(), name='option_question_trigger_detail'),
    url(r'^api/survey/(?P<survey_id>[\d]+)/question/$', views.QuestionList.as_view(), name='question_list'),

    #urls new design
    #url(r'^survey', TemplateView.as_view(template_name="index.html"), name='index'),
    url(r'^botchat/(?P<fb_page>.*)/(?P<survey_id_hash>.*)', views.redirect_to_chatbot, name='redirect_to_chatbot'),
    url(r'^api/survey/single_question/$', views.SingleQuestion.as_view(), name='single_question'),
    url(r'^api/survey/sq_result/(?P<survey_name>.*)$', views.SingleQuestionResult.as_view(), name='sq_result'),

    #urls useraccounts
    url(r'^user_accounts', TemplateView.as_view(template_name="user_accounts.html"), name='user_accounts'),
    url(r'^includes/admin_credentials_sidebar', TemplateView.as_view(template_name="includes/admin_credentials_sidebar.html"), name='admin_credentials_sidebar'),
    url(r'^admin_login', TemplateView.as_view(template_name="user/accounts/admin_login.html"), name='admin_login'),
    url(r'^admin_register', TemplateView.as_view(template_name="user/accounts/admin_register.html"), name='admin_register'),
    url(r'^admin_new_password', TemplateView.as_view(template_name="user/accounts/admin_new_password.html"), name='admin_new_password'),
    url(r'^admin_recover_password', TemplateView.as_view(template_name="user/accounts/admin_recover_password.html"), name='admin_recover_password'),
    url(r'^admin_add_code', TemplateView.as_view(template_name="user/accounts/admin_add_code.html"), name='admin_add_code'),


    #urls userscory_web
    url(r'^admin_configuration', TemplateView.as_view(template_name="user/admin_configuration.html"), name='admin_configuration'),
    url(r'^admin_dashboard', TemplateView.as_view(template_name="user/admin_dashboard.html"), name='admin_dashboard'),
    url(r'^admin_surveys', TemplateView.as_view(template_name="user/admin_surveys.html"), name='admin_surveys'),
    url(r'^survey_edit', TemplateView.as_view(template_name="user/survey_edit.html"), name='survey_edit'),
    url(r'^survey_config', TemplateView.as_view(template_name="user/survey_config.html"), name='survey_config'),
    url(r'^survey_jump', TemplateView.as_view(template_name="user/survey_jump.html"), name='survey_jump'),


    #urls includes
    url(r'^includes/admin_navbar', TemplateView.as_view(template_name="includes/admin_navbar.html"), name='admin_navbar'),
    url(r'^includes/survey_left_sidebar', TemplateView.as_view(template_name="includes/survey_left_sidebar.html"), name='survey_left_sidebar'),
    url(r'^includes/survey_navbar', TemplateView.as_view(template_name="includes/survey_navbar.html"), name='survey_navbar'),
    url(r'^includes/survey_left_general', TemplateView.as_view(template_name="includes/survey_left_general.html"), name='survey_left_general'),

    # catch-all pattern for compatibility with the Angular routes. This must be last in the list.
    #url(r'^(?P<path>.*)/$', TemplateView.as_view(template_name="index.html"), name='index'),
]