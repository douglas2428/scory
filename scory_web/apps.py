from __future__ import unicode_literals
from django.apps import AppConfig

class ScoryWebConfig(AppConfig):
    name = 'scory_web'