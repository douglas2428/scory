from django.contrib import admin

from scory_web.models import Contact

class ContactAdmin(admin.ModelAdmin):
	model = Contact
	list_display = ('name', 'mail', 'phone')
	ordering = ('name',)
	readonly_fields = ('name', 'mail', 'phone', 'company', 'message')

	class Media:
		css = {'all': ('scory_web/css/form_contact.css', )} 

admin.site.register(Contact, ContactAdmin)
