import hashlib
from rest_framework import serializers
from rest_framework.validators import UniqueTogetherValidator
from .models import Contact
from feedback.models import Survey, Type, OptionQuestion, OptionAnswer, Question
from app_bot.models import Company, Client
from _constants.constant import TYPE_LABEL_NAME


class ContactSerializer(serializers.ModelSerializer):
    class Meta:
        model = Contact
        fields = ('name', 'mail', 'phone', 'company', 'message')


class CompanySerializer(serializers.ModelSerializer):
    class Meta:
        model = Company
        fields = ('pk', 'name', 'identifier', 'latitude', 'longitude')


class SurveySerializer(serializers.ModelSerializer):
    company = CompanySerializer(read_only=True)

    class Meta:
        model = Survey
        fields = ('pk', 'name', 'company', 'required_time_expired', 'time_expired_minutes', 'start_message', 'end_message', 'url_messenger', 'url_messenger_code', 'created_at', 'updated_at', 'status')


class SurveySerializerPost(serializers.ModelSerializer):
    class Meta:
        model = Survey
        fields = ('pk', 'name', 'company', 'required_time_expired', 'time_expired_minutes', 'end_message', 'url_messenger', 'url_messenger_code', 'updated_at', 'status')


class TypeSerializer(serializers.ModelSerializer):
    label = serializers.SerializerMethodField()

    class Meta:
        model = Type
        fields = ('id', 'name', 'description', 'label')

    def get_label(self, obj):
        return TYPE_LABEL_NAME[obj.name]


class OptionAnswerSerializer(serializers.ModelSerializer):
    class Meta:
        model = OptionAnswer
        fields = ('pk', 'name', 'trigger_question', 'url_img')


class OptionQuestionSerializer(serializers.ModelSerializer):
    type = TypeSerializer(read_only=True)

    class Meta:
        model = OptionQuestion
        fields = ('id', 'question_text', 'survey', 'type', 'mandatory', 'sequence', 'options_answer')
        validators = [
            UniqueTogetherValidator(
                queryset=OptionQuestion.objects.all(),
                fields=('survey', 'sequence'),
                message="La secuencia no se puede repetir en la misma encuesta" #TODO agregar traduccion
            ),
        ]


class ClientSerializer(serializers.ModelSerializer):
    class Meta:
        model = Client
        fields = ('pk', 'first_name', 'last_name')


class QuestionSerializer(serializers.ModelSerializer):
    option_question = OptionQuestionSerializer(read_only=True)
    client = ClientSerializer(read_only=True)

    class Meta:
        model = Question
        fields = ('pk', 'option_question', 'answer', 'client', 'created_at')