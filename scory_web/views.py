# encoding=utf8
from serializers import ContactSerializer, CompanySerializer, SurveySerializer, SurveySerializerPost, TypeSerializer, OptionQuestionSerializer, OptionAnswerSerializer, QuestionSerializer
from rest_framework.views import APIView
from rest_framework.response import Response
from django.http import JsonResponse
from django.core import serializers
from rest_framework import status
from app_bot.models import Company
from feedback.models import Type, OptionQuestion, Survey, OptionAnswer, Question
import json
from rest_framework import generics
from django.db.models import F
import hashlib
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login
from django.utils import timezone
from app_bot.models import BotSetting, UserCompany
from app_bot.utils import *
import requests
import random
from django.conf import settings
from django.shortcuts import redirect


class SessionRestApi(APIView):

    def post(self, request, format=None):
        username = request.data.get('username', None)
        password = request.data.get('password', None)
        user = authenticate(username=username, password=password)
        if user is not None:
            if user.is_active:
                message = "Usuario válido, inicio de sesión exitoso"
                login(request, user)
                return Response({'message':message,'status': status.HTTP_200_OK})
            else:
                message = "La contraseña es válida, pero el usuario se encuentra inhabilitado."
        else:
            message = "Usuario o contraseña incorrectos."
        return Response({'message':message, 'status': status.HTTP_400_BAD_REQUEST})
        

class ContactRestApi(APIView):

    def post(self, request, format=None):
        serializer = ContactSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response({'contact': serializer.data, 'status': status.HTTP_201_CREATED})
        return Response({'error': serializer.errors, 'status': status.HTTP_400_BAD_REQUEST})


class CompanyList(generics.ListCreateAPIView):
    queryset = Company.objects.all().order_by('name')
    serializer_class = CompanySerializer


class TypeList(generics.ListCreateAPIView):
    queryset = Type.objects.all()
    serializer_class = TypeSerializer


class OptionQuestionSequenceNullBySurveyList(APIView):

    def get(self, request, survey_id, format=None):
        option_questions = OptionQuestion.objects.filter(survey__pk=survey_id, sequence__isnull=True)\
            .order_by('id')
        serializer = OptionQuestionSerializer(option_questions, many=True)
        return Response(serializer.data)


class OptionQuestionDetail(APIView):

    def put(self, request, survey_id, option_question_id, format=None):
        value = request.data.get('question_text',None)
        sequence_inserted = request.data.get('sequence', None)
        options_answer_to_save = request.data.get('listOptionsAnswerSave', [])
        options_answer_to_delete = request.data.get('listOptionsAnswerDelete', [])

        option_question_current = OptionQuestion.objects.get(pk=option_question_id)

        #options_answer to delete
        for option_answer in options_answer_to_delete:
            option_question_current.options_answer.remove(option_answer['id'])

        #options_answer to save
        list_options_answer_to_link = []
        for option_answer in options_answer_to_save:
            option_question_jump = None
            key_name = option_answer['name'].lower().replace(" ", "")
            if 'trigger_question' in option_answer and 'id' in option_answer['trigger_question']:
                key_name = "%s-%s" % (key_name, option_answer['trigger_question']['id'])
                option_question_jump = OptionQuestion.objects.get(pk=option_answer['trigger_question']['id'])
            elif 'trigger_question' in option_answer:
                if option_answer['trigger_question'] != '':
                    question_text = option_answer['trigger_question'].lower().replace(" ", "")
                    identifier = hashlib.md5()
                    identifier.update(question_text.encode('utf-8'))
                    unique_id_value = str(identifier.hexdigest())
                    
                    try:
                        survey_inserted = Survey.objects.get(id=survey_id)
                    except Survey.DoesNotExist:
                        print 'survey not exists'
                        return Response('No se encontro una Encuesta('+survey_id+')', status=status.HTTP_404_NOT_FOUND)

                    try:
                        option_question_jump = OptionQuestion.objects.get(unique_id=unique_id_value, survey=survey_inserted)
                    except OptionQuestion.DoesNotExist:
                        try:
                            type_question_jump = Type.objects.get(name='text')
                        except Type.DoesNotExist:
                            print 'type with name text not exists'
                            return Response('No se encontro un Type con name text', status=status.HTTP_404_NOT_FOUND)

                        option_question_jump = OptionQuestion.objects.create(question_text=option_answer['trigger_question'],
                                                      survey=survey_inserted,
                                                      type=type_question_jump,
                                                      mandatory=0,
                                                      sequence=None)
                        
            try:
                option_answer_current = OptionAnswer.objects.get(key=key_name, url_img=option_answer['url_img'], trigger_question=option_question_jump)
            except OptionAnswer.DoesNotExist:
                option_answer_current = OptionAnswer.objects.create(name=option_answer['name'],
                                                                    url_img=option_answer['url_img'],
                                                                    trigger_question=option_question_jump)
            
            list_options_answer_to_link.append(option_answer_current)

        for options_answer in list_options_answer_to_link:
            option_question_current.options_answer.add(options_answer)

        question_text = value.lower().replace(" ", "")
        identifier = hashlib.md5()
        identifier.update(question_text.encode('utf-8'))
        unique_id_value = str(identifier.hexdigest())

        if (OptionQuestion.objects.filter(pk=option_question_id).update(question_text=value, unique_id=unique_id_value)):
            Survey.objects.filter(pk=survey_id).update(updated_at=timezone.now())
            return Response(status=status.HTTP_200_OK)
        return Response("No se pudo actualizar la pregunta con id "+option_question_id+".", status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, survey_id, option_question_id, format=None):
        option_question_current = OptionQuestion.objects.filter(pk=option_question_id)
        sequence_current = option_question_current[0].sequence

        
        OptionQuestion.objects.filter(pk=option_question_id).delete()

        option_questions = OptionQuestion.objects.filter(survey__pk=survey_id, sequence__gt=sequence_current)\
            .order_by('sequence')

        for option_question in option_questions:
            OptionQuestion.objects.filter(pk=option_question.id).update(sequence=F('sequence') - 1)
        Survey.objects.filter(pk=survey_id).update(updated_at=timezone.now())
        return Response(status=status.HTTP_200_OK)   


class OptionQuestionBySurveyList(APIView):

    def get(self, request, survey_id, format=None):
        option_questions = OptionQuestion.objects.filter(survey__pk=survey_id, sequence__isnull=False)\
            .order_by('sequence')
        serializer = OptionQuestionSerializer(option_questions, many=True)
        return Response(serializer.data)

    def post(self, request, survey_id, format=None):
        sequence_inserted = request.data.get('sequence', None)
        type = request.data.get('type', None)
        option_question_updates = dict()
        options_answer_to_save = request.data.get('listOptionsAnswerSave', [])

        if type:
            type_name = type.get('name', None)
            if not type_name:
                return Response('No se ha proporcionado un Name para el Type enviado.', status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response('No se ha proporcionado el dato Type.', status=status.HTTP_400_BAD_REQUEST)

        try:
            type_inserted = Type.objects.get(name=type_name)
        except Type.DoesNotExist:
            print 'type not exists'
            return Response('No se encontro un Type de nombre '+type_name, status=status.HTTP_404_NOT_FOUND)

        try:
            survey_inserted = Survey.objects.get(id=survey_id)
        except Survey.DoesNotExist:
            print 'survey not exists'
            return Response('No se encontro una Encuesta('+survey_id+')', status=status.HTTP_404_NOT_FOUND)


        option_questions = OptionQuestion.objects.filter(survey__pk=survey_id, sequence__gte=sequence_inserted)\
                .order_by('-sequence')

        for option_question in option_questions:
            OptionQuestion.objects.filter(pk=option_question.id).update(sequence=F('sequence') +1)

        try:
            #options_answer to save
            list_options_answer_to_link_save = []
            for option_answer in options_answer_to_save:
                option_question_jump = None

                if 'trigger_question' in option_answer and 'id' in option_answer['trigger_question']:
                    option_question_jump = OptionQuestion.objects.get(pk=option_answer['trigger_question']['id'])
                elif 'trigger_question' in option_answer:
                    if option_answer['trigger_question'] != '':
                        question_text = option_answer['trigger_question'].lower().replace(" ", "")
                        identifier = hashlib.md5()
                        identifier.update(question_text.encode('utf-8'))
                        unique_id_value = str(identifier.hexdigest())

                        try:
                            option_question_jump = OptionQuestion.objects.get(unique_id=unique_id_value, survey=survey_inserted)
                        except OptionQuestion.DoesNotExist:

                            try:
                                type_question_jump = Type.objects.get(name='text')
                            except Type.DoesNotExist:
                                print 'type with name text not exists'
                                return Response('No se encontro un Type con name text', status=status.HTTP_404_NOT_FOUND)

                            option_question_jump = OptionQuestion.objects.create(question_text=option_answer['trigger_question'],
                                                                                 survey=survey_inserted,
                                                                                 type=type_question_jump,
                                                                                 mandatory=0,
                                                                                 sequence=None)
                
                option_answer_current = OptionAnswer.objects.create(name=option_answer['name'],
                                                                    url_img=option_answer['url_img'],
                                                                    trigger_question=option_question_jump)    
                list_options_answer_to_link_save.append(option_answer_current)
            
            option_question_inserted = OptionQuestion.objects.create(question_text=request.data.get('question_text', None),
                                                                     survey=survey_inserted,
                                                                     type=type_inserted,
                                                                     mandatory=0,
                                                                     sequence=sequence_inserted)

            Survey.objects.filter(pk=survey_id).update(updated_at=timezone.now())

            for options_answer in list_options_answer_to_link_save:
                option_question_inserted.options_answer.add(options_answer)
            
            serializer_option_question = OptionQuestionSerializer(option_question_inserted)

            #dict option_question with sequence update
            option_questions_sequence_update = OptionQuestion.objects.filter(survey__pk=survey_id, sequence__gt=sequence_inserted)
            for option_question in option_questions_sequence_update:
                option_question_updates[option_question.id] = option_question.sequence

            return Response({'dict':option_question_updates, 'option_question': serializer_option_question.data, 'status': status.HTTP_201_CREATED})

        except Exception as e:
            option_questions = OptionQuestion.objects.filter(survey__pk=survey_id, sequence__gt=sequence_inserted)\
                    .order_by('sequence')
            for option_question in option_questions:
                OptionQuestion.objects.filter(pk=option_question.id).update(sequence=F('sequence') -1)
            return Response(json.dumps(e), status=status.HTTP_400_BAD_REQUEST)




class UpdateOptionQuestionAndSequence(APIView):
    
    def put(self, request, survey_id, option_question_id, format=None):
        sequence = request.data.get('sequence',None)
        new_sequence = request.data.get('new_sequence',None)
        option_question_updates = dict()

        if sequence > new_sequence:
            OptionQuestion.objects.filter(pk=option_question_id).update(sequence=0)

            option_questions = OptionQuestion.objects.filter(survey__pk=survey_id, sequence__gte=new_sequence, sequence__lt=sequence)\
                .order_by('-sequence')

            for option_question in option_questions:
                option_question.sequence += 1
                option_question.save()
                option_question_updates[option_question.id] = option_question.sequence

        else:
            OptionQuestion.objects.filter(pk=option_question_id).update(sequence=0)

            option_questions = OptionQuestion.objects.filter(survey__pk=survey_id, sequence__gte=sequence, sequence__lte=new_sequence)\
                .order_by('sequence')

            for option_question in option_questions:
                option_question.sequence -= 1
                option_question.save()
                option_question_updates[option_question.id] = option_question.sequence

        OptionQuestion.objects.filter(pk=option_question_id).update(sequence=new_sequence)
        Survey.objects.filter(pk=survey_id).update(updated_at=timezone.now())
        option_question_updates[option_question_id] = new_sequence

        return Response(option_question_updates, status=status.HTTP_200_OK)

class SurveyList(APIView):

    def get(self, request, format=None):
        user_current = request.user
        surveys = Survey.objects.all()

        if not user_current.is_staff:
            surveys_filters = []
            for survey in surveys:
                users = survey.company.user.all()
                for user in users:
                    if user.id == user_current.id:
                        surveys_filters.append(survey)
            surveys = surveys_filters

        serializer = SurveySerializer(surveys, many=True)
        return Response(serializer.data)

    def post(self, request, format=None):
        serializer = SurveySerializerPost(data=request.data)
        if serializer.is_valid():
            serializer.save()
            
            survey_id = serializer.data['pk']

            obj_bot_setting = BotSetting.objects.first()

            url_messenger = generate_survey_bot_uri(survey_id, request.get_host())

            #get image code messenger
            obj = ManageThreadSettingsApi(obj_bot_setting.token)
            url_code_messenger = obj.get_url_image_code_by_param(param_encrypt, 1000)

            #Survey.objects.filter(pk=survey_id).update(url_messenger_code=url_code_messenger['uri'])
            Survey.objects.filter(pk=survey_id).update(url_messenger=url_messenger, url_messenger_code=url_code_messenger['uri'])

            return Response({'survey': serializer.data, 'status': status.HTTP_201_CREATED})
        return Response({'error': serializer.errors, 'status': status.HTTP_400_BAD_REQUEST})

class SurveyDetail(APIView):

    def get(self, request, survey_id, format=None):
        survey = Survey.objects.filter(pk=survey_id)
        serializer = SurveySerializer(survey, many=True)
        return Response(serializer.data)

    def put(self, request, survey_id, format=None):
        if (Survey.objects.filter(pk=survey_id).update(name=request.data.get('name',None),
                                                       company=request.data.get('company',None).get('id',None),
                                                       required_time_expired=request.data.get('required_time_expired',None),
                                                       time_expired_minutes=request.data.get('time_expired_minutes',None),
                                                       updated_at=timezone.now())):
            return Response(status= status.HTTP_200_OK)
        return Response({'error': "No se pudo actualizar la encuesta con id "+survey_id+".", 'status': status.HTTP_400_BAD_REQUEST})

    def delete(self, request, survey_id, format=None):
        try:
            Survey.objects.get(pk=survey_id).delete()
            return Response(status= status.HTTP_200_OK)
        except Survey.DoesNotExist:
            print 'survey not exists'
            return Response({'error': "No se pudo eliminar la encuesta con id "+survey_id, 'status': status.HTTP_400_BAD_REQUEST})


class OptionAnswersList(APIView):

    def get(self, request, format=None):
        options_answer = OptionAnswer.objects.all()
        serializer = OptionAnswerSerializer(options_answer, many=True)
        return Response(serializer.data)


class OptionAnswersByOptionQuestionList(APIView):

    def get(self, request, option_question_id, format=None):
        option_question_current = OptionQuestion.objects.get(pk=option_question_id)
        options_answer = option_question_current.options_answer
        serializer = OptionAnswerSerializer(options_answer, many=True)
        return Response(serializer.data)


class SurveyStatus(APIView):

    def put(self, request, survey_id, format=None):
        if (Survey.objects.filter(pk=survey_id).update(status=request.data.get('newStatus',None))):
            return Response(status= status.HTTP_200_OK)
        return Response({'error': "No se pudo actualizar la encuesta con id "+survey_id+".", 'status': status.HTTP_400_BAD_REQUEST})


class SurveyMessages(APIView):

    def put(self, request, survey_id, format=None):
        if (Survey.objects.filter(pk=survey_id).update(start_message=request.data.get('start_message',None),
                                                       end_message=request.data.get('end_message',None),
                                                       updated_at=timezone.now())):
            return Response(status= status.HTTP_200_OK)
        return Response({'error': "No se pudo actualizar la encuesta con id "+survey_id+".", 'status': status.HTTP_400_BAD_REQUEST})


class OptionQuestionTriggerBySurveyList(APIView):

    def get(self, request, survey_id, format=None):
        option_questions = OptionQuestion.objects.filter(survey__pk=survey_id, sequence__isnull=False)\
            .order_by('sequence')
        list_options_trigger = []
        for option_question in option_questions:
            if option_question.type.name == "multi_option":
                options_answers = option_question.options_answer.all()
                for opt in options_answers:
                    if opt.trigger_question:
                        list_options_trigger.append(opt.trigger_question)


        serializer = OptionQuestionSerializer(list_options_trigger, many=True)
        return Response(serializer.data)

    def post(self, request, survey_id, format=None):
        sequence_inserted = request.data.get('sequence', None)
        type = request.data.get('type', None)

        if type:
            type_name = type.get('name', None)
            if not type_name:
                return Response('No se ha proporcionado un Name para el Type enviado.', status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response('No se ha proporcionado el dato Type.', status=status.HTTP_400_BAD_REQUEST)

        try:
            type_inserted = Type.objects.get(name=type_name)
        except Type.DoesNotExist:
            print 'type not exists'
            return Response('No se encontro un Type de nombre '+type_name, status=status.HTTP_404_NOT_FOUND)

        try:
            survey_inserted = Survey.objects.get(id=survey_id)
        except Survey.DoesNotExist:
            print 'survey not exists'
            return Response('No se encontro una Encuesta('+survey_id+')', status=status.HTTP_404_NOT_FOUND)

        option_question_inserted = OptionQuestion.objects.create(question_text=request.data.get('question_text', None),
                                                                 survey=survey_inserted,
                                                                 type=type_inserted,
                                                                 mandatory=0,
                                                                 sequence=None)

        Survey.objects.filter(pk=survey_id).update(updated_at=timezone.now())
        serializer_option_question = OptionQuestionSerializer(option_question_inserted)
        return Response({'option_question': serializer_option_question.data, 'status': status.HTTP_201_CREATED})

class SurveyUrl(APIView):

    def put(self, request, survey_id, format=None):
        url_messenger = generate_survey_bot_uri(survey_id, request.get_host())
        
        obj_bot_setting = BotSetting.objects.first()
        obj = ManageThreadSettingsApi(obj_bot_setting.token)
        url_code_messenger = obj.get_url_image_code_by_param(param_encrypt, 1000)

        if (Survey.objects.filter(pk=survey_id).update(url_messenger=url_messenger, url_messenger_code=url_code_messenger['uri'])):
            return Response({'url_messenger': url_messenger, 'status': status.HTTP_200_OK})
        return Response({'error': "No se pudo generar url para la encuesta con id "+survey_id+".", 'status': status.HTTP_400_BAD_REQUEST})

    def post(self, request, survey_id, format=None):
        sequence_inserted = request.data.get('sequence', None)
        type = request.data.get('type', None)

        if type:
            type_name = type.get('name', None)
            if not type_name:
                return Response('No se ha proporcionado un Name para el Type enviado.', status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response('No se ha proporcionado el dato Type.', status=status.HTTP_400_BAD_REQUEST)

        try:
            type_inserted = Type.objects.get(name=type_name)
        except Type.DoesNotExist:
            print 'type not exists'
            return Response('No se encontro un Type de nombre '+type_name, status=status.HTTP_404_NOT_FOUND)

        try:
            survey_inserted = Survey.objects.get(id=survey_id)
        except Survey.DoesNotExist:
            print 'survey not exists'
            return Response('No se encontro una Encuesta('+survey_id+')', status=status.HTTP_404_NOT_FOUND)

        option_question_inserted = OptionQuestion.objects.create(question_text=request.data.get('question_text', None),
                                                                 survey=survey_inserted,
                                                                 type=type_inserted,
                                                                 mandatory=0,
                                                                 sequence=None)

        Survey.objects.filter(pk=survey_id).update(updated_at=timezone.now())
        serializer_option_question = OptionQuestionSerializer(option_question_inserted)
        # TODO el status code no esta correcto en este response, verificar todos los demas
        return Response({'option_question': serializer_option_question.data, 'status': status.HTTP_201_CREATED})


class OptionQuestionTriggerDetail(APIView):

    def put(self, request, survey_id, option_question_id, format=None):
        value = request.data.get('question_text',None)

        question_text = value.lower().replace(" ", "")
        identifier = hashlib.md5()
        identifier.update(question_text.encode('utf-8'))
        unique_id_value = str(identifier.hexdigest())

        if (OptionQuestion.objects.filter(pk=option_question_id).update(question_text=value, unique_id=unique_id_value)):
            Survey.objects.filter(pk=survey_id).update(updated_at=timezone.now())
            return Response(status=status.HTTP_200_OK)
        return Response("No se pudo actualizar la pregunta con id "+option_question_id+".", status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, survey_id, option_question_id, format=None):
        OptionQuestion.objects.filter(pk=option_question_id).delete()
        Survey.objects.filter(pk=survey_id).update(updated_at=timezone.now())
        return Response(status=status.HTTP_200_OK)

class QuestionList(APIView):

    def get(self, request, survey_id, format=None):
        questions = Question.objects.filter(option_question__survey_id=survey_id)
        serializer = QuestionSerializer(questions, many=True)
        count = []
        sequence = 2
        # only for single-question
        if questions and questions[0].option_question.survey.name[0:3] == 'SQ-':
            sequence = 1

        #FILTER SURVEY INICIADA
        for question in serializer.data:
            if question['option_question']['sequence'] == sequence:
                count.append(question['pk'])
        return Response(count)

        #FILTER CLIENT
        #for question in serializer.data:
        #    if not question['client'] in count and question['client']['first_name'] != 'undefined':
        #        count.append(question['client'])
        #return Response(count)


class SingleQuestion(APIView):
    def post(self, request, format=None):
        # crear company por default
        company, created = Company.objects.get_or_create(
            identifier='SQ',
            defaults={'name': 'SingleQuestion',
                      'latitude': -33.449825,
                      'longitude': -70.645467},)
        # crear survey
        start_message = 'Hola soy scory, una persona realizó la siguiente pregunta. Puedes responderla si lo deseas'
        end_message = 'Gracias por responder. Tu tambien puedes crear y compartir preguntas en www.scorybot.com'

        import uuid
        # creando survey name auto generado
        survey_name = "%s-%s" % ('SQ', uuid.uuid4().hex)
        new_survey = Survey.objects.create(
            name=survey_name,
            company=company,
            status='Activada',
            start_message=start_message,
            end_message=end_message,
            updated_at=timezone.now())

        opt1 = OptionAnswer.objects.create(name=request.data.get('optionAnswer1', None))
        opt2 = OptionAnswer.objects.create(name=request.data.get('optionAnswer2', None))

        type_sq = Type.objects.get(name='multi_option')

        oq = OptionQuestion.objects.create(
            question_text=request.data.get('question', None),
            survey=new_survey,
            type=type_sq,
            mandatory=0,
            sequence=1)
        oq.options_answer = [opt1, opt2]

        # generar el codigo url para compartir 
        # la encuesta (el mismo codigo del view surveyUrl view)
        # se debe colocar en una funcion aparte para ser reusada y no duplicar codigo
        url_messenger = generate_survey_bot_uri(new_survey.id, request.get_host())
        if url_messenger:
            return Response({'url_messenger': url_messenger, 'surveyhash': survey_name}, status=status.HTTP_200_OK)
        return Response({'error': "No se pudo generar url para la encuesta con id "+survey_id+"."}, status=status.HTTP_400_BAD_REQUEST)

class SingleQuestionResult(APIView):
    def get(self, request, survey_name, format=None):
        try:
            survey_sq = Survey.objects.get(name=survey_name)
        except Survey.DoesNotExist:
            Response(status=status.HTTP_404_NOT_FOUND)

        option_question = OptionQuestion.objects.filter(survey=survey_sq,
        type__name='multi_option').first()

        questions = Question.objects.filter(option_question=option_question)

        options = option_question.options_answer
        list_option = []

        for opt in options.all():
            count_opt = questions.filter(answer=opt.key).count()
            list_option.append({
                'name': opt.name,
                'count' : count_opt,
                'avg': (count_opt * 100 / questions.count()) if questions.count() > 0 else 0
                })
        list_answer = list(questions.values('client__first_name', 'client__last_name',
        'client__picture', 'created_at', 'answer'))
        return Response({'url_messenger': survey_sq.url_messenger, 'options': list_option,
        'list_answer': list_answer, 'question': option_question.question_text},
        status=status.HTTP_200_OK)

# mover funcion a algun paquete util
def generate_survey_bot_uri(survey_id, url_host):
    param = str(survey_id)
    param_encrypt = 'scory' + str(random.randint(111, 119)*random.randint(1, 8)) + param + str(random.randint(111, 119)*random.randint(1, 8)) 
    url_messenger = 'https://%s/scory/botchat/%s/%s' %(url_host, settings.USER_FB_PAGE, param_encrypt)
    
    obj_bot_setting = BotSetting.objects.first()
    obj = ManageThreadSettingsApi(obj_bot_setting.token)
    url_code_messenger = obj.get_url_image_code_by_param(param_encrypt, 1000)

    if (Survey.objects.filter(pk=survey_id).update(url_messenger=url_messenger,
    url_messenger_code=url_code_messenger['uri'])):
        return url_messenger
    return None

def redirect_to_chatbot(request, fb_page, survey_id_hash):
    return redirect('https://m.me/%s?ref=%s' %(fb_page, survey_id_hash))