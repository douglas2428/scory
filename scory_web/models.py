from __future__ import unicode_literals
from django.db import models


class Contact(models.Model):
    name = models.CharField(max_length=100)
    mail = models.EmailField(max_length=100)
    phone = models.CharField(max_length=25)
    company = models.CharField(max_length=100, blank=True)
    message = models.CharField(max_length=200, blank=True)

    def __str__(self):
        return self.name
