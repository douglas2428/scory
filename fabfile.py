# https://github.com/bmuller/fake
from contextlib import contextmanager as _contextmanager
from fake.api import *
from fake.tasks.deploy import *
import sys
env.setdefault('settings_env', '')

env.roledefs = {
    'develop': {
        'hosts': ['69.164.192.66'],
        'branch': 'develop',
        'deploy_path': '/home/deploy/develop_scory/dev_scory',
        'setting_env': 'alfredbot.settings.dev',
        'path_virtualenv': '/home/deploy/develop_scory/env_scory',
        'active_virtualenv': 'source /home/deploy/develop_scory/env_scory/bin/activate',
        'env_name': 'dev'
    },
    'production': {
        'hosts': ['69.164.192.66'],
        'branch': 'master',
        'deploy_path': '/home/deploy/production_scory/prod_scory',
        'setting_env': 'alfredbot.settings.prod',
        'path_virtualenv': '/home/deploy/production_scory/env_scory',
        'active_virtualenv': 'source /home/deploy/production_scory/env_scory/bin/activate',
        'env_name': 'prod'
    }
}

env.forward_agent = True
env.user = 'deploy'
env.repo_url = 'git@gitlab.com:douglas2428/scory.git'
@_contextmanager
def virtualenv():
    with cd(env.path_virtualenv):
        with prefix(env.active_virtualenv):
            yield
@task
def install_package():
    with virtualenv():
        with cd(paths.current_path):
            run('pip install -r requirements.txt')
@task
def migrate():
    with virtualenv():
        with cd(paths.current_path):
            run('./manage.py migrate --settings=%s' % env.setting_env)
@task
def collectstatic():
    with virtualenv():
        with cd(paths.current_path):
            run('./manage.py collectstatic --settings=%s' % env.setting_env)

@task
def ngbuild():
    with cd(paths.current_path):
        #run('curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.11/install.sh | bash', stderr=sys.stdout)
        #run('nvm install 8.12.0', stderr=sys.stdout)
        run('bash server_conf/%s_npm.sh' % env.env_name)
        
#before(started, ngbuild)
after(finished, install_package)
after(install_package, migrate)
after(migrate, ngbuild)
after(ngbuild, collectstatic)


# deploy to develop with
# fab -R develop deploy
# deploy to production with
# fab -R production deploy
# rollback with fab -R develop rollback
# rollback with fab -R production rollback