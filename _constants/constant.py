# encoding=utf8
IDENTIFIER_GET_STARTED_BUTTON = "get_started_button"
END_MESSAGE_SURVEY = "Gracias por dedicar tiempo en dar tu opinión :). Este establecimiento utilizará la calificación " \
                   "para mejorar sus productos y servicios; para ofrecerte un servicio de calidad :D"
MESSAGE_INACTIVITY = "Tiempo de inactividad cumplido, la encuesta se reiniciará."
JUMP_QUESTION = "saltar"

TYPE_LABEL_NAME = {"text": "Respuesta Libre".decode('utf-8'),
                   "multi_option": "Opción Múltiple".decode('utf-8'),
                   "location": "Localización".decode('utf-8')}
SURVEY_NOT_FOUND = "Disculpe, no hay encuestas registradas :(."
DEFAULT_COMPANY_IDENTIFIER_SURVEY = "BN"
KEY_GOOGLE_MAPS = "AIzaSyBDygoQjXxFaMjVDUKClTLFX2779Xw0yrY"