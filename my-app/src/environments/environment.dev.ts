export const environment = {
  production: true,
  api: {
    singleQuestionUrl : 'https://dev.scorybot.com/scory/api/survey/single_question/',
    singleQuestionResultUrl : 'https://dev.scorybot.com/scory/api/survey/sq_result'
  }
};

