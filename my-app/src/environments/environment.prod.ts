export const environment = {
  production: true,
  api: {
    singleQuestionUrl : 'https://www.scorybot.com/scory/api/survey/single_question/',
    singleQuestionResultUrl : 'https://www.scorybot.com/scory/api/survey/sq_result'
  }
};
