import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SurveyService {
  private headers: {};

  constructor(private http: HttpClient) {
    this.headers = {
      'headers':
        new HttpHeaders({
          'Content-Type': 'application/json'
        })
    };
  }

  createGenericOptions(survey: any) {
    return {
      method: 'POST',
      body: {
        question: survey.question,
        optionAnswer1: survey.optionAnswer1,
        optionAnswer2: survey.optionAnswer2
      },
      timeout: 10000,
      json: true,
      uri: null
    };
  }

  singleQuestion(survey: any) {
    return new Promise((resolve, reject) => {
      const options = this.createGenericOptions(survey);
      options.uri = environment.api.singleQuestionUrl;
      //console.log(options.body);
      this.http.post(options.uri, options.body, this.headers)
        .toPromise().then((response) => {
          // SUCCESS
          return resolve(response);
        })
        .catch((error) => {
          // ERROR
          return reject(error);
        });
    });
  };
}
