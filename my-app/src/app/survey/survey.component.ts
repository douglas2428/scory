import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { SurveyService } from '../survey/survey.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-survey',
  templateUrl: './survey.component.html',
  styleUrls: ['./survey.component.css']
})
export class SurveyComponent implements OnInit {
  title = 'results';
  surveyForm: FormGroup;
  isChecking: boolean;
  btDisabled: boolean;
  countCharsOpt1: number;
  countCharsOpt2: number;

  constructor(private router: Router,
    private fb: FormBuilder,
    private surveyService: SurveyService,
    private spinner: NgxSpinnerService
  ) {
    this.createForm();
  }

  createForm() {
    this.surveyForm = this.fb.group({
      question: ['', Validators.compose([Validators.required, Validators.minLength(5)])],
      optionAnswer1: ['', Validators.required ],
      optionAnswer2: ['', Validators.required ]
    });
  }

  ngOnInit() {
    this.btDisabled = false;
    this.isChecking = false;
    this.countCharsOpt1 = 50;
    this.countCharsOpt2 = 50;
  }

  countDownCharOpt1(event: any) {
    if (this.countCharsOpt1 >= 0 && this.countCharsOpt1 <= 50){
      this.countCharsOpt1 = 50 - this.surveyForm.value.optionAnswer1.length
    }
  }

  countDownCharOpt2(event: any) {
    if (this.countCharsOpt2 >= 0 && this.countCharsOpt2 <= 50){
      this.countCharsOpt2 = 50 - this.surveyForm.value.optionAnswer2.length
    }
  }

  createSurvey(survey: any) {
    this.isChecking = true;
    if (this.surveyForm.valid) {
      if(survey.question.indexOf('?') === -1)
         survey.question = survey.question + '?';
      if(survey.question.indexOf('¿') === -1)
         survey.question = '¿' + survey.question;
      this.spinner.show();
      this.btDisabled = true;
      this.surveyService
        .singleQuestion(survey)
        .then((resp) => {
          // SUCCESS
          this.router.navigate(['/scory/result/', resp['surveyhash']]);
        })
        .catch(error => {
          console.log(error);
          this.isChecking = false;
          this.spinner.hide();
        });
    }
    else{
      console.log('Error en el contenido del formulario')
    }
  }
}