import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { ClipboardModule } from 'ngx-clipboard';
import { NgxSpinnerModule } from 'ngx-spinner';
import { NgbModalModule } from '@ng-bootstrap/ng-bootstrap';

import { AppComponent } from './app.component';
import { RouterModule, Routes } from '@angular/router';
import { PageNotFoundComponent} from './page-not-found/page-not-found.component';
import { SurveyComponent } from './survey/survey.component';
import { ResultComponent } from './result/result.component';
import { ShareModalComponent } from './share-modal/share-modal.component';

const appRoutes: Routes = [
  { path: 'scory/survey', component: SurveyComponent },
  { path: 'scory/result/:surveyHash', component: ResultComponent },
  { path: '',
    redirectTo: '/scory/survey',
    pathMatch: 'full'
  },
  { path: 'scory',
    redirectTo: '/scory/survey',
    pathMatch: 'full'
  },
  { path: '**', redirectTo: '/scory/survey' }
  //{ path: '**', component: PageNotFoundComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    SurveyComponent,
    ResultComponent,
    PageNotFoundComponent,
    ShareModalComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    ClipboardModule,
    NgxSpinnerModule,
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: false } // <-- true debugging purposes only
    ),
    NgbModalModule.forRoot()
  ],
  entryComponents: [ShareModalComponent],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
