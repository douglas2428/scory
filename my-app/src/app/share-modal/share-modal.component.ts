import { Component, OnInit, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { HelperService } from '../helpers/helpers-service';

@Component({
  selector: 'app-share-modal',
  templateUrl: './share-modal.component.html',
  styleUrls: ['./share-modal.component.css']
})
export class ShareModalComponent implements OnInit {
  @Input() question: string;
  @Input() options: any;
  @Input() urlMessenger: string;
  urlShareFacebook: string;
  urlShareTwitter: string;
  urlShareWhatsapp: string;
  copyMessage: string;

  constructor(public activeModal: NgbActiveModal,
    public helperService: HelperService) { 
      this.copyMessage = 'Copiar link'
    }

  ngOnInit() {
    const url = this.urlMessenger;
    const boldQuestion = this.helperService.getBoldTextForStr(this.question);
    const text = `Mira esta pregunta :D, ${boldQuestion}, elige una opción aquí`;
    const hashtag = 'preguntas, encuestas';
    const textWp = encodeURIComponent(`${text} ${url}`)
    this.urlShareFacebook = `https://www.facebook.com/sharer/sharer.php?u=${url}$t=${this.question}&quote=${text}&hashtags=${hashtag}`;
    this.urlShareTwitter = `https://twitter.com/intent/tweet?url=${url}&text=${text}&hashtags=${hashtag}&via=scorybot`;
    this.urlShareWhatsapp = `https://wa.me/?text=${textWp}`;
  }

  onClickCopyLink(event: any): void {
    event.stopPropagation();
    event.preventDefault();
  }

  updateCopyMessage() {
    this.copyMessage = 'Copiado!';
    setTimeout(() => {
      this.copyMessage = 'Copiar link';
    }, 2000);
  }

}
