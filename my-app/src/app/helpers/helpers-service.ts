import {Injectable} from "@angular/core";
import { TextMap } from "./text-map";
  
@Injectable({
  providedIn: 'root'
})
export class HelperService {
  constructor() {}
  getBoldTextForStr(str: String){
    return str.split('').map(s => TextMap.BoldMap[s] || s).join('')
  }
}