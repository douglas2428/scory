import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ResultService {
  private headers: {};

  constructor(private http: HttpClient) {
    this.headers = {
      'headers':
        new HttpHeaders({
          'Content-Type': 'application/json'
        })
    };
  }

  createGenericOptions(surveyHash) {
    const url = environment.api.singleQuestionResultUrl;
    return {
      method: 'GET',
      timeout: 10000,
      json: true,
      uri: `${url}/${surveyHash}`
    };
  }

  singleQuestionResult(surveyHash: string) {
    return new Promise((resolve, reject) => {
      const options = this.createGenericOptions(surveyHash);
      this.http.get(options.uri, this.headers)
        .toPromise().then((response) => {
          return resolve(response);
        })
        .catch((error) => {
          return reject(error);
        });
    });
  };
}
