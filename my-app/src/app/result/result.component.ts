import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ResultService } from '../result/result.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap'
import { ShareModalComponent } from '../share-modal/share-modal.component';


@Component({
  selector: 'app-result',
  templateUrl: './result.component.html',
  styleUrls: ['./result.component.css']
})
export class ResultComponent implements OnInit {
  urlMessenger: string
  options: any
  question: string
  messagePlural: object
  listAnswer: object[]
  urlResult: string
  copyMessage: string
  urlMsgbot: string

  constructor(private resultService: ResultService,
    private router: Router,
    private route: ActivatedRoute,
    private spinner: NgxSpinnerService,
    private sanitizer: DomSanitizer,
    private modal: NgbModal) {
      this.messagePlural = {
        'answer': {
          '=1' : '1 Respuesta',
          'other' : '# Respuestas',
        },
        'result': {
          '=1' : '1 Resultado',
          'other' : '# Resultados',
        }
      }
      this.listAnswer = []
      this.urlResult = window.location.href;
      this.copyMessage = 'Copiar';
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      const surveyHash = params['surveyHash'];
      this.spinner.show();
      this.resultService.singleQuestionResult(surveyHash)
        .then((resp) => {
          this.urlMessenger = resp['url_messenger'];
          this.options = resp['options'];
          this.question = resp['question'];
          this.listAnswer = resp['list_answer'];

          this.urlMsgbot = this.question + '\n\n vota tu opción en scory chatbot: ' + this.urlMessenger

          this.spinner.hide();
          this.openShareModal();
        })
        .catch(error => {
          console.log('some error');
          this.spinner.hide();
        });
    });
  }

  redirectCreateSurvey(){
    this.router.navigate(['/scory/survey/']);
  }

  updateCopyMessage(){
    this.copyMessage = 'Copiado!';
    setTimeout(() => {
      this.copyMessage = 'Copiar';
    }, 2000);
  }

  sanitizerBase64Img(imgBase64){
    return this.sanitizer.bypassSecurityTrustResourceUrl('data:image/png;base64, ' + imgBase64);
  }

  openShareModal(){
    const modalRef = this.modal.open(ShareModalComponent);
    modalRef.componentInstance.question = this.question;
    modalRef.componentInstance.options = this.options;
    modalRef.componentInstance.urlMessenger = this.urlMessenger;
  }

}
