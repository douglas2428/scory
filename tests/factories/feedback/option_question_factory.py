import factory
from feedback.models import OptionQuestion
from survey_factory import SurveyFactory


class OptionQuestionFactory(factory.DjangoModelFactory):

    class Meta:
        model = OptionQuestion

    survey = factory.SubFactory(SurveyFactory)
    question_text = factory.Sequence(lambda n: 'question_text%d' % n)
