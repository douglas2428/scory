import factory
from feedback.models import Survey

from tests.factories.app_bot.company_factory import CompanyFactory


class SurveyFactory(factory.DjangoModelFactory):

    class Meta:
        model = Survey

    name = factory.Sequence(lambda n: 'survey_%d' % n)
    company = factory.SubFactory(CompanyFactory)
