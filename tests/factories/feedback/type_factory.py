import factory
from feedback.models import Type


class TypeFactory(factory.DjangoModelFactory):

    class Meta:
        model = Type

    name = factory.Sequence(lambda n: 'type_%d' % n)
    description = factory.Sequence(lambda n: 'description_%d' % n)