import factory
from feedback.models import OptionAnswer
from option_question_factory import OptionQuestionFactory


class OptionAnswerFactory(factory.DjangoModelFactory):

    class Meta:
        model = OptionAnswer

    trigger_question = factory.SubFactory(OptionQuestionFactory)
    name = factory.Sequence(lambda n: 'name%d' % n)