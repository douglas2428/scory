import factory
from factory import fuzzy
from feedback.models import Company


class CompanyFactory(factory.DjangoModelFactory):

    class Meta:
        model = Company

    name = factory.Sequence(lambda n: 'company_%d' % n)
    identifier = factory.Sequence(lambda n: 'identifier_%d' % n)
    latitude = fuzzy.FuzzyDecimal(low=-90.0, high=90.0, precision=5)
    longitude = fuzzy.FuzzyDecimal(low=-180.0, high=180.0, precision=5)
    place_id = factory.Sequence(lambda n: 'place_id_%d' % n)

