import unittest
from feedback.models import OptionQuestion, Survey, Type, OptionAnswer
from tests.factories.feedback.option_question_factory import OptionQuestionFactory
from tests.factories.feedback.survey_factory import SurveyFactory
from tests.factories.feedback.type_factory import TypeFactory
from tests.factories.feedback.option_answer_factory import OptionAnswerFactory
from django.core.exceptions import ValidationError
from app_bot.models import Company
from tests.factories.app_bot.company_factory import CompanyFactory


class SurveyTestCase(unittest.TestCase):

    def setUp(self):

        self.company = CompanyFactory.build(name="Company", identifier="Comp", latitude=10.153605, longitude=-67.999313)
        self.company.place_id = self.company._place_id()
        self.company.save()
        self.survey = SurveyFactory.create(name="Survey Default", company=self.company)

        self.question_type = TypeFactory.create()
        self.option_question1 = OptionQuestionFactory.create(sequence=1, survey=self.survey, type=self.question_type)
        self.option_question3 = OptionQuestionFactory.create(sequence=3, survey=self.survey, type=self.question_type)
        self.option_question2 = OptionQuestionFactory.create(sequence=2, survey=self.survey, type=self.question_type)

    def tearDown(self):
        Survey.objects.all().delete()
        Type.objects.all().delete()
        OptionQuestion.objects.all().delete()
        Company.objects.all().delete()

    def test_survey_get_next_question_by_question(self):
        """
        check survey.get_next_question by an option_question with sequence should return the next
        option_question
        :return:
        """
        data_returned = self.survey.get_next_question(option_question=self.option_question2)

        self.assertEqual(data_returned, self.option_question3)

    def test_survey_get_next_question_by_explicit_sequence(self):
        """
        check survey.get_next_question by option_question without sequence but with parameter sequence
        should return the next option_question for this sequence
        :return:
        """
        option_question_without_sequence = OptionQuestionFactory.create(sequence=None, survey=self.survey,
                                                                        type=self.question_type)
        data_returned = self.survey.get_next_question(option_question=option_question_without_sequence,
                                                    sequence=1)

        self.assertEqual(data_returned, self.option_question2)

    def test_survey_get_next_question_by_last_question(self):
        """
        check survey.get_next_question by last option_question should return None
        :return:
        """

        data_returned = self.survey.get_next_question(option_question=self.option_question3)

        self.assertEqual(data_returned, None)

    def test_survey_get_next_question_without_params(self):
        """
        check get_next_question without parameter should return the first optionQuestion from the survey
        :return:
        """
        data_returned = self.survey.get_next_question()

        self.assertEqual(data_returned, self.option_question1)

    def test_create_survey_equal_name_and_company(self):
        survey1 = SurveyFactory.create(name='Survey 1', company=self.company)
        survey2 = SurveyFactory.build(name='Survey 1', company=self.company)
        with self.assertRaises(ValidationError) as cm:
            survey2.clean()
            survey2.save()
        self.assertEqual(Survey.objects.filter(name='Survey 1').count(), 1)
        self.assertEqual(cm.exception[0], 'Name survey in use for this company.')
        self.assertEqual(cm.exception.code, 'name_survey_used')
 
    def test_create_survey_distinct_name_and_equal_company(self):
        company = CompanyFactory.build(name="Company C.A", identifier="Comp C.A", latitude=10.076056, longitude=-69.116504)
        company.place_id = self.company._place_id()
        company.save()
        survey1 = SurveyFactory.create(name='Survey 1', company=company)
        survey2 = SurveyFactory.create(name='Survey 2', company=company)
        self.assertEqual(Survey.objects.filter(company=company).count(), 2)

    def test_create_survey_less_time_expired_minutes(self):
        company = CompanyFactory.build(name="Company C.A", identifier="Comp C.A", latitude=10.076056, longitude=-69.116504)
        company.place_id = self.company._place_id()
        company.save()
        survey = SurveyFactory.build(name='Survey 1', company=company, required_time_expired=True, time_expired_minutes=-1)
        with self.assertRaises(ValidationError) as cm:
            survey.clean()
            survey.save()
        self.assertEqual(Survey.objects.filter(name='Survey 1').count(), 0)
        self.assertEqual(cm.exception[0], 'Expiration time less than one minute.')
        self.assertEqual(cm.exception.code, 'min_time_expired')

    def test_create_survey_time_expired_minutes(self):
        company = CompanyFactory.build(name="Company C.A", identifier="Comp C.A", latitude=10.076056, longitude=-69.116504)
        company.place_id = self.company._place_id()
        company.save()
        survey = SurveyFactory.create(name='Survey 1', company=company, required_time_expired=True, time_expired_minutes=20)
        self.assertEqual(Survey.objects.filter(name='Survey 1').count(), 1)


class OptionQuestionTestCase(unittest.TestCase):

    def setUp(self):
        self.company = CompanyFactory.build(name="Company", identifier="Comp", latitude=10.153605, longitude=-67.999313)
        self.company.place_id = self.company._place_id()
        self.company.save()
        self.survey = SurveyFactory.create(name="Survey Default", company=self.company)
        self.question_type = TypeFactory.create()
        self.option_question1 = OptionQuestionFactory.create(survey=self.survey, question_text='Esta es pregunta 1?', type=self.question_type, sequence=1)
        self.option_question2 = OptionQuestionFactory.create(survey=self.survey, question_text='Esta es pregunta 2?', type=self.question_type, sequence=2)
        
    def tearDown(self):
        Survey.objects.all().delete()
        Type.objects.all().delete()
        OptionQuestion.objects.all().delete()
        Company.objects.all().delete()

    def test_equal_unique_id(self):
        option_question_equal_question_text = OptionQuestionFactory.build(survey=self.survey, question_text='estaes pregunta1?', type=self.question_type, sequence=1)
        data_returned = option_question_equal_question_text._unique_id()
        self.assertEqual(data_returned, self.option_question1._unique_id())

    def test_different_unique_id(self):
        data_returned = self.option_question1._unique_id()
        self.assertNotEqual(data_returned, self.option_question2._unique_id())

    def test_create_option_question_equal_survey_and_unique_id(self):
        data_returned = OptionQuestionFactory.build(survey=self.survey, question_text='estaes pregunta1?', type=self.question_type, sequence=3)
        with self.assertRaises(ValidationError) as cm:
            data_returned.clean()
            data_returned.save()
        self.assertEqual(OptionQuestion.objects.filter(question_text='estaes pregunta 1?').count(), 0)
        self.assertEqual(cm.exception[0], 'Question already exists.')
        self.assertEqual(cm.exception.code, 'already_exists')
        
    def test_create_option_question_equal_survey_and_sequence(self):
        data_returned = OptionQuestionFactory.build(survey=self.survey, question_text='Esta es pregunta 3?', type=self.question_type, sequence=2)
        with self.assertRaises(ValidationError) as cm:
            data_returned.clean()
            data_returned.save()
        self.assertEqual(OptionQuestion.objects.filter(sequence=2).count(), 1)
        self.assertEqual(cm.exception[0], 'Sequencia en uso para el Survey seleccionado.')
        self.assertEqual(cm.exception.code, 'sequence_used')

    def test_update_option_question_equal_survey_and_unique_id(self):
        self.option_question2.question_text = 'Esta es pregunta 1?'
        with self.assertRaises(ValidationError) as cm:
            self.option_question2.clean()
            self.option_question2.save()
        self.assertEqual(cm.exception[0], 'Question already exists.')
        self.assertEqual(cm.exception.code, 'already_exists')

    def test_update_option_question_equal_survey_and_sequence(self):
        self.option_question2.sequence = 1
        with self.assertRaises(ValidationError) as cm:
            self.option_question2.clean()
            self.option_question2.save()
        self.assertEqual(cm.exception[0], 'Sequencia en uso para el Survey seleccionado.')
        self.assertEqual(cm.exception.code, 'sequence_used')

    def test_update_option_question_distinct_unique_id_and_sequence_null(self):
        option_question_sequence_null = OptionQuestionFactory.create(survey=self.survey, question_text='Pregunta incorrecta?', type=self.question_type, sequence=None)
        option_question3 = OptionQuestionFactory.create(survey=self.survey, question_text='Es la pregunta 3?', type=self.question_type, sequence=3)
        option_question3.sequence = None
        option_question3.clean()
        option_question3.save()
        self.assertEqual(OptionQuestion.objects.filter(sequence=None).count(), 2)

    def test_update_option_question_distinct_unique_id_and_sequence(self):
        self.option_question2.question_text = 'Nueva pregunta?'
        self.option_question2.sequence = 100
        self.option_question2.clean()
        self.option_question2.save()
        self.assertEqual(OptionQuestion.objects.filter(question_text='Nueva pregunta?').count(), 1)
        self.assertEqual(OptionQuestion.objects.filter(sequence=100).count(), 1)

class OptionAnswerTestCase(unittest.TestCase):

    def setUp(self):
        self.company = CompanyFactory.build(name="Company", identifier="Comp", latitude=10.153605, longitude=-67.999313)
        self.company.place_id = self.company._place_id()
        self.company.save()
        self.survey = SurveyFactory.create(name="Survey Default", company=self.company)
        self.question_type = TypeFactory.create()
        self.option_question = OptionQuestionFactory.create(survey=self.survey, question_text='Primera pregunta?', type=self.question_type, sequence=1)
        self.option_answer1 = OptionAnswerFactory.create(name='Esta es mi primera opcion', trigger_question=self.option_question, url_img=None)
        self.option_answer2 = OptionAnswerFactory.create(name='Esta es mi segunda opcion', trigger_question=self.option_question, url_img=None)

    def tearDown(self):
        Survey.objects.all().delete()
        Type.objects.all().delete()
        OptionQuestion.objects.all().delete()
        OptionAnswer.objects.all().delete()
        Company.objects.all().delete()

    def test_equal_key(self):
        option_answer_equal_name = OptionAnswerFactory.build(name='esta esmi primeraopcion', trigger_question=self.option_question, url_img=None)
        data_returned = option_answer_equal_name._key()
        self.assertEqual(data_returned, self.option_answer1._key())

    def test_different_key(self):
        data_returned = self.option_answer1._key()
        self.assertNotEqual(data_returned, self.option_answer2._key())

    def test_create_option_answer_equal_name_and_trigger_question(self):
        option_answer_equal_name = OptionAnswerFactory.build(name='esta esmi primeraopcion', trigger_question=self.option_question, url_img=None)
        with self.assertRaises(ValidationError) as cm:
            option_answer_equal_name.clean()
            option_answer_equal_name.save()
        self.assertEqual(cm.exception[0], 'OptionAnswer already exists.')
        self.assertEqual(cm.exception.code, 'already_exists')
        self.assertEqual(OptionAnswer.objects.filter(name="esta esmi primeraopcion").count(), 0)

    def test_create_option_answer_distinct_trigger_question_equal_name(self):
        option_question_distinct = OptionQuestionFactory.create(survey=self.survey, question_text='Segunda Pregunta?', type=self.question_type, sequence=None)
        option_answer_equal_name = OptionAnswerFactory.create(name='esta esmi primeraopcion', trigger_question=option_question_distinct, url_img=None)
        self.assertEqual(OptionAnswer.objects.filter(trigger_question=option_question_distinct).count(), 1)

    def test_update_option_answer_equal_name_and_trigger_question(self):
        self.option_answer2.name = 'Esta es mi primera opcion'
        with self.assertRaises(ValidationError) as cm:
            self.option_answer2.clean()
            self.option_answer2.save()
        self.assertEqual(cm.exception[0], 'OptionAnswer already exists.')
        self.assertEqual(cm.exception.code, 'already_exists')
        self.assertEqual(OptionAnswer.objects.filter(name='Esta es mi primera opcion').count(), 1)

    def test_update_option_answer_distinct_name_equal_trigger_question(self):
        self.option_answer2.name = 'Esta es otra respuesta'
        self.option_answer2.clean()
        self.option_answer2.save()
        self.assertEqual(OptionAnswer.objects.filter(name='Esta es otra respuesta').count(), 1)

    def test_update_option_answer_distinct_trigger_question_equal_name(self):
        option_question_distinct = OptionQuestionFactory.create(survey=self.survey, question_text='Es Otra Pregunta?', type=self.question_type, sequence=None)
        self.option_answer2.trigger_question = option_question_distinct
        self.option_answer2.clean()
        self.option_answer2.save()
        self.assertEqual(OptionAnswer.objects.filter(trigger_question=option_question_distinct).count(), 1)