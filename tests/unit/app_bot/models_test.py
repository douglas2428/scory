import unittest
from app_bot.models import Company
from django.core.exceptions import ValidationError
from tests.factories.app_bot.company_factory import CompanyFactory

class CompanyTestCase(unittest.TestCase):

    def setUp(self):
        self.company = CompanyFactory.build(name="Company 2", identifier="C1", latitude=10.153605, longitude=-67.999313)
        self.company.place_id = self.company._place_id()
        self.company.save()

    def tearDown(self):
        Company.objects.all().delete()

    def test_create_company_equal_identifier(self):
        company2 = CompanyFactory.build(name="Company 2", identifier="C1", latitude=10.153605, longitude=-67.999313)
        company2.place_id = company2._place_id()
        with self.assertRaises(ValidationError) as cm:
            company2.clean()
            company2.save()
        self.assertEqual(Company.objects.filter(identifier='C1').count(), 1)
        self.assertEqual(cm.exception[0], 'Identifier already exists.')
        self.assertEqual(cm.exception.code, 'identifier_used')

    def test_create_company_equal_coordinates(self):
        company2 = CompanyFactory.build(name="Company 2", identifier="C2", latitude=10.153605, longitude=-67.999313)
        company2.place_id = company2._place_id()
        with self.assertRaises(ValidationError) as cm:
            company2.clean()
            company2.save()
        self.assertEqual(Company.objects.filter(identifier='C2',name="Company 2").count(), 0)
        self.assertEqual(cm.exception[0], 'Coordinates in use by another company.')
        self.assertEqual(cm.exception.code, 'coordinates_used')

    def test_create_company_distinct_identifier_and_coordinates(self):
        company1 = CompanyFactory.build(name="Company Uno", identifier="CU", latitude=10.076056, longitude=-69.116504)
        company1.place_id = company1._place_id()
        company2 = CompanyFactory.build(name="Company Dos", identifier="CD", latitude=10.153605, longitude=-67.999313)
        company2.place_id = company2._place_id()
        company1.save()
        company2.save()
        self.assertEqual(Company.objects.filter(name="Company Uno").count(), 1)
        self.assertEqual(Company.objects.filter(name="Company Dos").count(), 1)