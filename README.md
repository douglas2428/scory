# alfredbot

# Instalación

- Instalar virtualenv
- Instalar http://cffi.readthedocs.io/en/latest/installation.html
- Instalar https://cryptography.io/en/latest/installation/
- Instalar todos los paquetes requeridos en requirements.txt

# Instalar postgresql y configurar la base de datos
$ sudo apt-get update
$ sudo apt-get install postgresql postgresql-contrib
$ sudo apt-get install python-dev libpq-dev
- Ingresar al virtuelenv e instalar requirements.txt (En este caso para instalar el paquete psycopg2 si no ha sido instalado)
- Ingresar al directorio fooddy/server_conf/
- ejecutar el archivo create_app_db.sh
$ sh ./create_app_db.sh
- aplicar las migraciones
Si se quiere, cargar los datos de la encuesta predefinida /feedback/encuesta_barra_norte.json
$ ./manage.py loaddata --settings="alfredbot.settings.development" feedback/encuesta_barra_norte.json


# Integracion con nuevo AngulaJS
Para ejecutar el servidor angular, ubicarse en el directorio my-app y luego ejecutar el comando ng serve --open.
Se abrira la web con los nuevos cambios.

Tambien se debe ejecutar el servidor django en otra terminal para que se comuniquen ambas.

* Para que la web funcione solo ejecutando el servidor django se debe ejecutar desde my-app el comando ng build
* Esto permite compilar todos los cambios realizados en los archivos .ts
* Luego se debe copiar los .js generados en my-app y pegarlo scory/static/ng5/js (con el comando npm run build:copy  se hace el build y la copia de archivos)
* de esa forma django podra servir los archivos staticos de angular y solo sera neceario ejecutar el servidor django.
* (El problema es que las imagenes no las podra encontrar dinamicamente con tag static de django, ya que en los .ts no
* se puede usar los tag de templates de django. Hay q ver como resolver esto)
