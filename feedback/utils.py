# encoding=utf8
import requests
from _constants.constant import KEY_GOOGLE_MAPS


def get_location_by_coordinates(lat,lng):
    post_message_url = 'https://maps.googleapis.com/maps/api/geocode/json?latlng=%s,%s&key=%s' %(lat, lng, KEY_GOOGLE_MAPS)
    resp = requests.post(post_message_url, headers={"Content-Type": "application/json"})
    content = resp.json()
    dictionary = dict()
    if content['status']=="OK":
        dictionary = {'place_id': content['results'][0]['place_id'], 'formatted_address': content['results'][0]['formatted_address']}
    return dictionary