from __future__ import unicode_literals
import hashlib
from django.db import models
from app_bot.models import Company, Client
from django.utils import timezone
from django.utils.encoding import python_2_unicode_compatible
from django.core.exceptions import ValidationError
import datetime
from django.utils.timezone import utc
from django.utils.safestring import mark_safe
from django.template.defaultfilters import truncatechars


class Survey(models.Model):
    name = models.CharField(max_length=50)
    company = models.ForeignKey(Company)
    required_time_expired = models.BooleanField(default=0)
    time_expired_minutes = models.IntegerField(default=30)
    url_messenger = models.TextField(default='', blank=True, null=True)
    url_messenger_code = models.TextField(default='', blank=True, null=True)
    start_message = models.TextField(default='', blank=True, null=True)
    end_message = models.TextField(default='', blank=True, null=True)
    created_at = models.DateTimeField(default=timezone.now, blank=True)
    updated_at = models.DateTimeField(default=timezone.now, blank=True)
    status = models.CharField(default='Desactivada', max_length=20)

    def image_messenger_code(self):
        return mark_safe('<img id="id_img_messenger_code" src="%s" width="150" height="150" />' % (self.url_messenger_code))

    image_messenger_code.short_description = 'Image messenger code'

    def __unicode__(self):
        return self.name

    def get_next_question(self, option_question=None, sequence=None):

        if option_question and isinstance(option_question, OptionQuestion):
            # check if the option_question sequence or parameter sequence are not None
            if option_question.sequence:
                sequence = option_question.sequence
            if sequence:
                option_questions = self.optionquestion_set.filter(sequence=sequence + 1)
                if option_questions:
                    return option_questions.first()
            else:
                #TODO check this validation. for sequence when is the first question or the last question
                return self.optionquestion_set.exclude(sequence__isnull=True).order_by("sequence").first()
            return None

        else:
            return self.optionquestion_set.exclude(sequence__isnull=True).order_by("sequence").first()

    def get_minute_inactivity_survey(self, question):
        if self.required_time_expired:
            time_inactivity = datetime.datetime.utcnow().replace(tzinfo=utc) - question.created_at.replace(tzinfo=utc)
            time_inactivity_minutes = time_inactivity.seconds / 60
            return time_inactivity_minutes >= self.time_expired_minutes
        return False

    def clean(self):
        conflicting_name_survey = Survey.objects.filter(name=self.name, company=self.company).exclude(id=self.id)
        if conflicting_name_survey.exists():
            raise ValidationError('Name survey in use for this company.', code = 'name_survey_used')
        if self.required_time_expired and self.time_expired_minutes < 1:
            raise ValidationError('Expiration time less than one minute.', code = 'min_time_expired')


class Type(models.Model):
    name = models.CharField(max_length=50, unique=True)
    description = models.TextField()

    def __unicode__(self):
        return self.name


class OptionQuestion(models.Model):
    unique_id = models.CharField(max_length=50)
    question_text = models.TextField()
    survey = models.ForeignKey(Survey)
    type = models.ForeignKey(Type)
    mandatory = models.BooleanField(default=0)
    sequence = models.IntegerField(blank=True, null=True)
    options_answer = models.ManyToManyField('OptionAnswer', blank=True)

    class Meta:
        unique_together = (('survey', 'unique_id',), ('survey', 'sequence'),)

    def __unicode__(self):
        return self.question_text

    def _unique_id(self):
        # create unique_id for the question
        question_text = self.question_text.lower().replace(" ", "")
        identifier = hashlib.md5()
        identifier.update(question_text.encode('utf-8'))
        unique_id_value = str(identifier.hexdigest())
        return  unique_id_value

    def save(self, *args, **kwargs):
        self.unique_id = self._unique_id()
        super(OptionQuestion, self).save(*args, **kwargs)

    def clean(self):
        conflicting_instance = OptionQuestion.objects.filter(survey=self.survey, unique_id=self._unique_id()).exclude(id=self.id)
        if conflicting_instance.exists():
            raise ValidationError('Question already exists.', code='already_exists')

        conflicting_sequence_survey = OptionQuestion.objects.filter(survey=self.survey, sequence=self.sequence).exclude(id=self.id)
        if conflicting_sequence_survey.exists():
            if self.sequence != None:
                raise ValidationError('Sequencia en uso para el Survey seleccionado.', code='sequence_used')

    def question_text_preview(self):
        return truncatechars(self.question_text, 100)


class Question(models.Model):
    option_question = models.ForeignKey(OptionQuestion)
    answer = models.TextField()
    client = models.ForeignKey(Client)
    created_at = models.DateTimeField(default=timezone.now, blank=True)

    def __unicode__(self):
        return "%s-%s" % (self.option_question, self.client.first_name)

    def client_full_name(self):
        return self.client.first_name+' '+self.client.last_name

    def survey_name(self):
        return self.option_question.survey.name

    def option_question_preview(self):
        return truncatechars(self.option_question, 70)

    def company_name(self):
        return self.option_question.survey.company.name


@python_2_unicode_compatible
class OptionAnswer(models.Model):
    key = models.CharField(max_length=100)
    name = models.CharField(max_length=50)
    trigger_question = models.ForeignKey(OptionQuestion, blank=True, null=True,
                                         limit_choices_to={'sequence__isnull': True},)
    url_img = models.CharField(max_length=200, blank=True, null=True)

    def __str__(self):
        return "%s/%s" % (self.name,
                          (self.trigger_question.question_text if self.trigger_question else "No trigger question"))

    def _key(self):
        name = self.name.lower().replace(" ", "")
        if self.trigger_question:
            return "%s-%s" % (name, self.trigger_question.id)
        else:
            return name

    def save(self, *args, **kwargs):
        self.key = self._key()
        super(OptionAnswer, self).save(*args, **kwargs)

    def clean(self):
        conflicting_instance = OptionAnswer.objects.filter(key=self._key()).exclude(id=self.id)
        if conflicting_instance.exists():
            raise ValidationError('OptionAnswer already exists.', code = 'already_exists')


