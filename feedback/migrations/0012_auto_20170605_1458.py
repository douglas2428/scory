# -*- coding: utf-8 -*-
# Generated by Django 1.10.6 on 2017-06-05 14:58
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('feedback', '0011_optionquestion_mandatory'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='Poll',
            new_name='Survey',
        ),
        migrations.RenameField(
            model_name='optionquestion',
            old_name='poll',
            new_name='survey',
        ),
        migrations.AlterUniqueTogether(
            name='optionquestion',
            unique_together=set([('survey', 'sequence'), ('survey', 'unique_id')]),
        ),
    ]
