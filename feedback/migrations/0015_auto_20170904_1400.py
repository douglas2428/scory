# -*- coding: utf-8 -*-
# Generated by Django 1.10.6 on 2017-09-04 14:00
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('feedback', '0014_survey_end_message'),
    ]

    operations = [
        migrations.AlterField(
            model_name='optionanswer',
            name='key',
            field=models.CharField(max_length=100),
        ),
        migrations.AlterField(
            model_name='optionquestion',
            name='unique_id',
            field=models.CharField(max_length=50),
        ),
    ]
