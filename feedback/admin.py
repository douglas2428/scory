from django.contrib import admin
from django import forms
from feedback.models import Survey, Type, OptionQuestion, Question, OptionAnswer
from django.core.exceptions import ValidationError
from django.http import  HttpResponse
from app_bot.models import Client


def question_export_xlsx(modeladmin, request, queryset):
    import openpyxl
    from openpyxl.utils import get_column_letter
    from openpyxl.styles import Font
    response = HttpResponse(content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
    response['Content-Disposition'] = 'attachment; filename=question.xlsx'
    wb = openpyxl.Workbook()
    ws = wb.get_active_sheet()
    ws.title = "Question List"

    row_num = 0

    columns = [
        ("Company", 30),
        ("Survey", 30),
        ("Question", 40),
        ("Answer", 40),
        ("User", 30),
        ("Date", 20),
    ]

    for col_num in xrange(len(columns)):
        c = ws.cell(row=row_num + 1, column=col_num + 1)
        c.font = Font(bold=True)
        c.value = columns[col_num][0]
        ws.column_dimensions[get_column_letter(col_num+1)].width = columns[col_num][1]

    for obj in queryset:
        row_num += 1
        option_question = OptionQuestion.objects.get(id=obj.option_question_id)
        user = Client.objects.get(id=obj.client_id)
        row = [
            option_question.survey.company.name,
            option_question.survey.name,
            option_question.question_text,
            obj.answer,
            user.first_name+' '+user.last_name,
            obj.created_at,
        ]
        for col_num in xrange(len(row)):
            c = ws.cell(row=row_num + 1, column=col_num + 1)
            c.value = row[col_num]

    wb.save(response)
    return response

question_export_xlsx.short_description = "Export XLSX"

class SurveyAdmin(admin.ModelAdmin):
    readonly_fields = ('id','image_messenger_code',)
    list_display = ('name', 'first_question')

    def first_question(self, obj):
        return obj.optionquestion_set.first().question_text


    class Media:
        js = ['js/admin_hidden_fields.js']


class TypeAdmin(admin.ModelAdmin):
    pass


class QuestionAdmin(admin.ModelAdmin):
    actions = [question_export_xlsx]
    list_display = ('company_name', 'survey_name', 'option_question_preview', 'answer', 'client_full_name', 'created_at')
    ordering = ('option_question__survey__name','client_id__first_name','created_at')
    readonly_fields = ('client','client_full_name','option_question','answer','created_at')
    search_fields = ['option_question__survey__company__name', 'option_question__survey__name', 'client_id__first_name', 'created_at',]
    list_filter = ('option_question__survey__company__name', 'option_question__survey__name', 'client_id__first_name')
    class Media: 
        css = {'all': ('css/question.css', )} 


class OptionAnswerAdmin(admin.ModelAdmin):
    exclude = ('key',)
    list_display = ('name', 'trigger_question', 'url_img')


class OptionQuestionAdminForm(forms.ModelForm):
    class Meta:
        model = OptionQuestion
        exclude = ('unique_id',)

    def __init__(self, *args, **kwargs):
        super(OptionQuestionAdminForm, self).__init__(*args, **kwargs)
        self.fields['options_answer'].queryset = OptionAnswer.objects.exclude(
            trigger_question__unique_id=self.instance.unique_id)

    def clean(self):
        if self.cleaned_data['type'].name == "multi_option":
            if not self.cleaned_data['options_answer'].first():
                raise ValidationError("Seleccione al menos una OptionAnswer, cuando tipo sea multi_option", code = 'multi_option_selected')
        

class OptionQuestionAdmin(admin.ModelAdmin):
    form = OptionQuestionAdminForm
    filter_horizontal = ('options_answer',)
    list_display = ('question_text_preview', 'type', 'sequence', 'survey')
    ordering = ('survey','sequence')

    class Media:
        js = ['js/admin_hidden_fields.js']


admin.site.register(Survey, SurveyAdmin)
admin.site.register(Type, TypeAdmin)
admin.site.register(OptionQuestion, OptionQuestionAdmin)
admin.site.register(Question, QuestionAdmin)
admin.site.register(OptionAnswer, OptionAnswerAdmin)

