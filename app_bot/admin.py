from django.contrib import admin
from app_bot.models import Order, Client, FeedBack, Company, BotSetting, Message, UserCompany
from django import forms
from django.core.exceptions import ValidationError
from django.contrib.auth.models import User


class OrderAdmin(admin.ModelAdmin):
    pass


class ClientAdmin(admin.ModelAdmin):
    list_display = ('facebook_user_id', 'first_name', 'last_name')


class FeedBackAdmin(admin.ModelAdmin):
    pass


class CompanyAdminForm(forms.ModelForm):
    class Meta:
        model = Company
        exclude = ('unique_id',)

    def __init__(self, *args, **kwargs):
        super(CompanyAdminForm, self).__init__(*args, **kwargs)
        self.fields['user'].queryset = User.objects.exclude(
            id=self.instance.id)

    def clean(self):
        if not self.cleaned_data['user'].first():
            raise ValidationError("Seleccione al menos un Usuario, para esta compania", code = 'user_company_selected')


class CompanyAdmin(admin.ModelAdmin):
    exclude = ('place_id',)
    form = CompanyAdminForm
    filter_horizontal = ('user',)

    class Media:
        js = ['js/modal_map.js']

    def save_model(self, request, obj, form, change):
        obj.save()
        current_user = request.user
        UserCompany.objects.create(user=current_user, company=obj)

        
class BotSettingAdmin(admin.ModelAdmin):
    exclude = ('updated_at',)
    list_display = ('token', 'initial_greeting')

    class Media:
        js = ['js/bot_initial_greeting.js']


class MessageAdmin(admin.ModelAdmin):
    list_display = ('created_at', 'company_name','survey_name','question', 'client_full_name',  'user_message', 'bot_message')
    ordering = ('created_at',)
    readonly_fields = ('user_message', 'bot_message', 'created_at')

    class Media:
		css = {'all': ('css/form_message.css', )} 
 
admin.site.register(Order, OrderAdmin)
admin.site.register(Client, ClientAdmin)
admin.site.register(FeedBack, FeedBackAdmin)
admin.site.register(Company, CompanyAdmin)
admin.site.register(BotSetting, BotSettingAdmin)
admin.site.register(Message, MessageAdmin)
