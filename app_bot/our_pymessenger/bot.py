from pymessenger.bot import Bot
import logging
import requests


class Bot(Bot):

    def __init__(self, *args, **kwargs):
        super(Bot, self).__init__(*args, **kwargs)

    def send_quick_replies_location(self, recipient_id, text, is_attachment=False):
        """Send  quick-replies to the specified recipient.
        https://developers.facebook.com/docs/messenger-platform/send-api-reference/quick-replies
        Input:
            recipient_id: recipient id to send to
            elements: generic message elements to send
        Output:
            Response from API as <dict>
        """
        quick_replies = [{
            "content_type": "location"
        }]

        return self.send_quick_replies(recipient_id, text, quick_replies, is_attachment)

    def send_quick_replies_labels(self, recipient_id, text, quick_replies, is_attachment=False):
        """Send  quick-replies to the specified recipient.
        https://developers.facebook.com/docs/messenger-platform/send-api-reference/quick-replies
        Input:
            recipient_id: recipient id to send to
            elements: generic message elements to send
        Output:
            Response from API as <dict>
        """
        json_quick_replies = []
        for qr in quick_replies:
            if not isinstance(qr, QuickReplyLabel):
                logging.error("%s is not instance of QuickReplyLabel" %qr)
            else:
                json_quick_replies.append(qr.get_in_json_format())

        return self.send_quick_replies(recipient_id, text, json_quick_replies, is_attachment)

    def send_quick_replies(self, recipient_id, text, quick_replies, is_attachment=False):
        """Send  quick-replies to the specified recipient.
        https://developers.facebook.com/docs/messenger-platform/send-api-reference/quick-replies
        Input:
            recipient_id: recipient id to send to
            elements: generic message elements to send
        Output:
            Response from API as <dict>
        """

        payload = {
            'recipient': {
                'id': recipient_id
            },
            'message': {
                "quick_replies": quick_replies
            }
        }
        if is_attachment:
            payload["message"]["attachment"] = text
        else:
            payload["message"]["text"] = text

        return self.send_raw(payload)

    def send_update_settings_thread_state(self, menu_items):
        """Send request to set and update the configuration of the main menu.
        https://developers.facebook.com/docs/messenger-platform/thread-settings/persistent-menu
        Input:
            menu_items: list of dict menu_item
            # example menu_item: {
                                  "type":"postback",
                                  "title":"Start a New Order",
                                  "payload":"DEVELOPER_DEFINED_PAYLOAD_FOR_START_ORDER"
                                }
        Output:
            Response from API as <dict>
        """

        payload = {
            "setting_type": "call_to_actions",
            "thread_state": "existing_thread",
            "call_to_actions": menu_items
        }

        return self.api_post_thread_settings(payload)

    def delete_settings_main_menu(self):
        """Send request to delete the configuration of the main menu.
        https://developers.facebook.com/docs/messenger-platform/thread-settings/persistent-menu
        """

        payload = {
            "setting_type": "call_to_actions",
            "thread_state": "existing_thread"
        }

        return self.api_delete_thread_settings(payload)

    def send_update_settings_greeting(self, greeting_text):
        """Send request to set and update the initial greeting text.
        https://developers.facebook.com/docs/messenger-platform/thread-settings/greeting-text
        Input:
            greeting_text: String
            # example greeting_text:
        Output:
            Response from API as <dict>
        """

        payload = {
            "setting_type": "greeting",
            "greeting": {
                "text": greeting_text
            }
        }

        return self.api_post_thread_settings(payload)

    def delete_settings_greeting(self):
        """Send request to delete the initial greeting text.
        https://developers.facebook.com/docs/messenger-platform/thread-settings/greeting-text
        """

        payload = {
            "setting_type": "greeting"
        }

        return self.api_delete_thread_settings(payload)

    def send_update_get_started_button(self, identifier_get_started_button):
        """Send request to set and update the get-started-button.
        https://developers.facebook.com/docs/messenger-platform/thread-settings/get-started-button
        Input:
            identifier_get_start_button: string
            # example identifier_get_start_button: "get_start_button"
        Output:
            Response from API as <dict>
        """

        payload = {
            "setting_type": "call_to_actions",
            "thread_state": "new_thread",
            "call_to_actions": [
                {
                    "payload": identifier_get_started_button
                }
            ]
        }

        return self.api_post_thread_settings(payload)

    def delete_get_started_button(self):
        """Send request to delete the get-started-button.
        https://developers.facebook.com/docs/messenger-platform/thread-settings/get-started-button
        """

        payload = {
            "setting_type": "call_to_actions",
            "thread_state": "new_thread"
        }

        return self.api_delete_thread_settings(payload)

    def api_post_thread_settings(self, payload):
        request_endpoint = '{0}/me/thread_settings'.format(self.graph_url)
        response = requests.post(
            request_endpoint,
            params=self.auth_args,
            json=payload
        )
        result = response.json()
        return result

    def api_delete_thread_settings(self, payload):
        request_endpoint = '{0}/me/thread_settings'.format(self.graph_url)
        response = requests.delete(
            request_endpoint,
            params=self.auth_args,
            json=payload
        )
        result = response.json()
        return result

    def api_generate_messeger_code_param(self, param_ref, image_size=1000):
        """Send request to get the url image code with parameter ref.
        https://developers.facebook.com/docs/messenger-platform/messenger-code
        """
        request_endpoint = '{0}/me/messenger_codes'.format(self.graph_url)

        payload = {
            "type": "standard",
            "data": {
                    "ref": param_ref
                },
            "image_size": image_size
        }

        response = requests.post(
            request_endpoint,
            params=self.auth_args,
            json=payload
        )

        result = response.json()
        return result

    def update_access_token(self, page_id, page_key, exchange_token):
        request_endpoint = "https://graph.facebook.com/v2.12/oauth/access_token?grant_type=fb_exchange_token&client_id="+page_id+"&client_secret="+page_key+"&fb_exchange_token="+exchange_token+"&access_token"
        response = requests.get(
            request_endpoint
        )
        result = response.json()
        return result

class QuickReplyLabel(object):

    def __init__(self, title, payload, image_url=None, content_type="text"):
        self.title = title
        self.payload = payload
        self.image_url = image_url
        self.content_type = content_type

    def get_in_json_format(self):
        """

        :return:
        """

        quick_reply = {
            "content_type": self.content_type,
            "title": self.title,
            "payload": self.payload,
        }

        if self.image_url:
            quick_reply["image_url"] = self.image_url

        return quick_reply
