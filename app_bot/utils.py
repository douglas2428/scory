from our_pymessenger.bot import Bot
from _constants.constant import IDENTIFIER_GET_STARTED_BUTTON


class ManageThreadSettingsApi(object):

    def __init__(self, token):
        self.bot = Bot(token)

    def send_setting_main_menu(self):
        """
        Call this method in order to update a button main menu with these menu_items
        :return:
        """
        menu_items = [
            {
                "type": "postback",
                "title": "Ayuda",
                "payload": "DEVELOPER_DEFINED_PAYLOAD_FOR_HELP"
            },
            {
                "type": "postback",
                "title": "Iniciar nueva evaluacion",
                "payload": "DEVELOPER_DEFINED_PAYLOAD_FOR_START_SURVEY"
            }
        ]
        response = self.bot.send_update_settings_thread_state(menu_items)
        print (response)

    def delete_setting_main_menu(self):
        """
        Call this method in order to delete button main menu
        :return:
        """
        response = self.bot.delete_settings_main_menu()
        print response

    def send_initial_greeting(self, text=None):
        """
        Call this method in order to set or update the initial greeting text
        text: string (160 characters max)
        :return:
        """

        if not text:
            text = "Hola {{user_first_name}}, soy Fooddy, ayudo a los locales amigos a " \
                   "brindarte un mejor servicio. Preciona Empezar para que califiquemos juntos:)"
        else:
            text = text.replace("@user_facebook", "{{user_first_name}}")


        response = self.bot.send_update_settings_greeting(text)
        print (response)

    def delete_initial_greeting(self):
        """
        Call this method in order to delete the initial greeting text
        :return:
        """
        response = self.bot.delete_settings_greeting()
        print response

    def send_update_get_started_button(self):
        """
        Call this method in order to set or update the get-started-button
        :return:
        """

        response = self.bot.send_update_get_started_button(IDENTIFIER_GET_STARTED_BUTTON)
        print (response)

    def delete_get_started_button(self):
        """
        Call this method in order to delete the get-started-button
        :return:
        """
        response = self.bot.delete_get_started_button()
        print response

    def get_url_image_code_by_param(self, param_ref, image_size=1000):
        """
        Call this method in order generate and get the url image code with parameter ref include
        param_ref: string (250 characters max)  valid characters: "a-z A-Z 0-9 +/=-.:_"
        :return:
        """
        response = self.bot.api_generate_messeger_code_param(param_ref, image_size)
        return response

    def update_access_token_timed_out(self, page_id, page_key, exchange_token):
        """
        Call this method to renew the access_token with at least 20 days of execution in the bot_settings
        :return:
        """
        response = self.bot.update_access_token(page_id, page_key, exchange_token)
        return response
