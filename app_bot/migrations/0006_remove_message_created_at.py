# -*- coding: utf-8 -*-
# Generated by Django 1.10.2 on 2016-11-13 18:15
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app_bot', '0005_auto_20161113_1810'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='message',
            name='created_at',
        ),
    ]
