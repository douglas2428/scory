# -*- coding: utf-8 -*-
# Generated by Django 1.10.6 on 2018-02-14 22:27
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('app_bot', '0015_auto_20180213_0314'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='usercompany',
            name='company',
        ),
        migrations.RemoveField(
            model_name='usercompany',
            name='user',
        ),
        migrations.AddField(
            model_name='company',
            name='user',
            field=models.ManyToManyField(blank=True, related_name='_company_user_+', to=settings.AUTH_USER_MODEL),
        ),
        migrations.DeleteModel(
            name='UserCompany',
        ),
    ]
