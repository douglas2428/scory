# -*- coding: utf-8 -*-
# Generated by Django 1.10.2 on 2016-11-13 02:32
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app_bot', '0002_auto_20161108_0317'),
    ]

    operations = [
        migrations.AlterField(
            model_name='message',
            name='created_at',
            field=models.DateTimeField(),
        ),
    ]
