# -*- coding: utf-8 -*-
# Generated by Django 1.10.6 on 2018-02-08 05:29
from __future__ import unicode_literals

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('app_bot', '0013_company_place_id'),
    ]

    operations = [
        migrations.AddField(
            model_name='botsetting',
            name='updated_at',
            field=models.DateTimeField(blank=True, default=datetime.datetime(2018, 1, 17, 5, 29, 27, 357596, tzinfo=utc)),
        ),
    ]
