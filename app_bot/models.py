from __future__ import unicode_literals
from django.db import models
from django.utils import timezone
import utils
from _constants.constant import DEFAULT_COMPANY_IDENTIFIER_SURVEY
from feedback.utils import get_location_by_coordinates
from django.core.exceptions import ValidationError
from django.contrib.auth.models import User
from django.conf import settings


class Company(models.Model):
    place_id = models.SlugField(max_length=255)
    name = models.CharField(max_length=50)
    identifier = models.CharField(max_length=50)
    latitude = models.DecimalField(max_digits=9, decimal_places=6)
    longitude = models.DecimalField(max_digits=9, decimal_places=6)
    user = models.ManyToManyField(User, blank=True, related_name='+')

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name_plural = "companies"

    def _place_id(self):
        company_location = get_location_by_coordinates(self.latitude, self.longitude)
        return company_location['place_id']

    def save(self, *args, **kwargs):
        self.place_id = self._place_id()
        super(Company, self).save(*args, **kwargs)

    def clean(self):
        conflicting_identifier= Company.objects.filter(identifier=self.identifier).exclude(id=self.id)
        if conflicting_identifier.exists() and self.identifier != DEFAULT_COMPANY_IDENTIFIER_SURVEY:
            raise ValidationError('Identifier already exists.', code = 'identifier_used')

        conflicting_coordinates = Company.objects.filter(place_id=self._place_id()).exclude(id=self.id)
        if conflicting_coordinates.exists():
            raise ValidationError('Coordinates in use by another company.', code = 'coordinates_used')


class UserCompany(models.Model):
    user = models.ForeignKey(User)
    company = models.ForeignKey(Company)


class Client(models.Model):
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    gender = models.CharField(max_length=10, default='')
    picture = models.TextField()
    facebook_user_id = models.CharField(max_length=50)

    def __unicode__(self):
        return self.facebook_user_id


class Order(models.Model):
    identifier = models.CharField(max_length=50)
    client = models.ForeignKey(Client)
    company = models.ForeignKey(Company)
    created_at = models.DateTimeField()
    ready_at = models.DateField(null=True, blank=True)
    finish_at = models.DateField(null=True, blank=True)

    def __unicode__(self):
        return "%s-%s" % (self.identifier,self.client)

    def add_message(self, message):
        Message.objects.create(order=self,message=message)


class Message(models.Model):
    question = models.ForeignKey('feedback.Question', null=True, blank=True)
    bot_message = models.TextField(null=True, blank=True)
    user_message = models.TextField(null=True, blank=True)
    created_at = models.DateTimeField(default=timezone.now)
    survey = models.ForeignKey('feedback.Survey', null=True, blank= True)
    client = models.ForeignKey(Client, null=True, blank= True)

    def __unicode__(self):
        return unicode(self.bot_message) or u''

    def company_name(self):
       return self.survey.company.name

    def survey_name(self):
        return self.survey.name

    def client_full_name(self):
        if self.client and self.client.first_name != 'undefined':
            return self.client.first_name+' '+self.client.last_name
        return ''


class FeedBack(models.Model):
    order = models.ForeignKey(Order)
    points = models.IntegerField()

    def __unicode__(self):
        return self.order

def default_time():
    now = timezone.now()
    return now - timezone.timedelta(days=20)  

class BotSetting(models.Model):
    token = models.TextField()
    initial_greeting = models.TextField(blank=True, null=True)
    started_button = models.BooleanField(default=0)
    updated_at = models.DateTimeField(default=default_time, blank=True)

    def __unicode__(self):
        return self.token

    

    def clean(self):
        bot_setting = BotSetting.objects.last()
        self.updated_at = timezone.now()
        if bot_setting.initial_greeting != self.initial_greeting or bot_setting.token != self.token:
            if self.initial_greeting == '' :
                utils.ManageThreadSettingsApi(self.token).delete_initial_greeting()
            else:
                utils.ManageThreadSettingsApi(self.token).send_initial_greeting(text=self.initial_greeting)

        if bot_setting.started_button != self.started_button or bot_setting.token != self.token:
            if self.started_button:
                utils.ManageThreadSettingsApi(self.token).send_update_get_started_button()
            else:
                utils.ManageThreadSettingsApi(self.token).delete_get_started_button()


class ClientSurvey(models.Model):
    client = models.ForeignKey(Client)
    survey = models.ForeignKey('feedback.Survey')
    created_at = models.DateTimeField(default=timezone.now)

    def __unicode__(self):
        return self.client.facebook_user_id