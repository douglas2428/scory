# encoding=utf8
from __future__ import unicode_literals
import json
import requests
import logging
import base64
from django.http import HttpResponse
from django.views.generic.base import View
from django.views.decorators.csrf import csrf_exempt
from django.utils.decorators import method_decorator
from django.views import generic
from our_pymessenger.bot import Bot, QuickReplyLabel
from alfredbot.settings.base import WEBHOOKS_FB_VERIFY_TOKEN
from _constants.constant import IDENTIFIER_GET_STARTED_BUTTON, END_MESSAGE_SURVEY, MESSAGE_INACTIVITY, JUMP_QUESTION, SURVEY_NOT_FOUND, DEFAULT_COMPANY_IDENTIFIER_SURVEY
from app_bot.models import Client,  BotSetting, ClientSurvey, Message
from feedback.models import Survey, Question
from django.shortcuts import render_to_response
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from app_bot.utils import *
from django.utils import timezone
from django.conf import settings
import random


class FacebookMessenger(View):

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return generic.View.dispatch(self, request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        if 'hub.verify_token' in self.request.GET and self.request.GET['hub.verify_token'] == WEBHOOKS_FB_VERIFY_TOKEN:
            return HttpResponse(self.request.GET['hub.challenge'])
        else:
            return HttpResponse("error")

    def post(self, request, *args, **kwargs):
        bot_setting = BotSetting.objects.last()
        bot = Bot(bot_setting.token)
        
        #update_access_token
        current_time = timezone.now() - bot_setting.updated_at 
        if current_time.days > 20:
            bot_setting = BotSetting.objects.first()
            obj = ManageThreadSettingsApi(bot_setting.token)
            response = obj.update_access_token_timed_out(settings.PAGE_ID, settings.PAGE_KEY, bot_setting.token)
            BotSetting.objects.update(token=response['access_token'], updated_at=timezone.now())
            bot_setting = BotSetting.objects.last()
            bot = Bot(bot_setting.token)

        incoming_message = json.loads(self.request.body.decode('utf-8'))
        for entry in incoming_message['entry']:
            for message in entry['messaging']:
                if 'sender' in message and 'id' in message['sender']:
                    user_id = str(message['sender']['id'])
                    client = create_facebook_client(user_id, bot_setting.token)
                    survey = get_survey(client)
                    # process referral 
                    if 'referral' in message and 'ref' in message['referral']:
                        # Show the bot typing
                        bot.send_action(str(user_id), "typing_on")
                        param_survey_id = message['referral']['ref']

                        #decrypting params survey_id
                        param_survey_id = param_survey_id[:len(param_survey_id) - 3]
                        param_survey_id = param_survey_id[8:len(param_survey_id)]

                        survey = create_client_survey(client,param_survey_id)
                        process_answer(request, client, message, bot, survey=survey, start_survey=True)
                    # process only message or postback
                    elif message.get('message', None) or message.get('postback', None):
                        # Show the bot typing
                        bot.send_action(str(user_id), "typing_on")
                        if 'message' in message:
                            # process message
                            process_answer(request, client, message, bot, survey=survey)
                        # process postback
                        elif 'postback' in message:
                            if 'payload' in message['postback']:
                                if 'referral' in message['postback'] and 'ref' in message['postback']['referral']:
                                    param_survey_id = message['postback']['referral']['ref']

                                    #decrypting params survey_id
                                    param_survey_id = param_survey_id[:len(param_survey_id) - 3]
                                    param_survey_id = param_survey_id[8:len(param_survey_id)]

                                    survey = create_client_survey(client,param_survey_id)
                                    process_answer(request, client, message, bot, survey=survey, start_survey=True)
                                elif message['postback']['payload'] == IDENTIFIER_GET_STARTED_BUTTON:
                                    process_answer(request, client, "", bot, survey=survey, start_survey=True)
        return HttpResponse("AlfredBot")


def get_user_profile(user_id, token):
    endpoint_profile = 'https://graph.facebook.com/v3.1/%s?fields=first_name,last_name,picture&access_token=%s' % (user_id, token)
    response = requests.get(endpoint_profile)
    if response.status_code == 200:
        return response.json()
    logging.info("Unable to get user profile of user_id %s" % user_id)
    return {}

def create_client_survey(client,survey_id):
    try:
        survey = Survey.objects.get(pk=survey_id)
    except Survey.DoesNotExist:
        survey = Survey.objects.filter(company__identifier=DEFAULT_COMPANY_IDENTIFIER_SURVEY).first()
    if survey:
        ClientSurvey.objects.create(client=client, survey=survey)
    return survey

def get_survey(client):
    client_survey = ClientSurvey.objects.filter(client=client.id).order_by('-created_at').first()
    if not client_survey:
        survey = Survey.objects.filter(company__identifier=DEFAULT_COMPANY_IDENTIFIER_SURVEY).first()
        return survey
    return client_survey.survey

def get_as_base64(url):
    return base64.b64encode(requests.get(url).content)
    
def create_facebook_client(user_id, token):
    update = False
    try:
        client = Client.objects.get(facebook_user_id=user_id)
        if client.first_name == 'undefined' or not client.picture or client.picture == 'undefined':
            update = True
        else:
            return client
    except Client.DoesNotExist:
        client = None

    json_user = get_user_profile(user_id, token)
    first_name = json_user.get("first_name", 'undefined')
    last_name = json_user.get("last_name", 'undefined')
    gender = json_user.get("gender", 'undefined')
    picture = json_user.get("picture", {}).get("data", {}).get("url", "")
    picture_base64 = get_as_base64(picture)

    if update:
        if first_name != 'undefined':
            client.first_name = first_name
        if last_name != 'undefined':
            client.last_name = last_name
        if gender != 'undefined':
            client.gender = gender
        if picture:
            client.picture = picture_base64
        client.save()
    else:
        client = Client.objects.create(facebook_user_id=user_id,
                                       first_name=first_name,
                                       last_name=last_name,
                                       gender=gender,
                                       picture=picture_base64)
    return client


def process_location_answer(answer):
    """
    Verificar y procesar la respuesta de localizacion del usuario
    :param answer:
    :return: dict con answer_ok = True y data = ({lat: 3000, long: 3000})
    Formato del json que retorna facebook con la respuesta de la localizacion del usuario:
        {
        u'message':
             {u'attachments':
                  [{u'payload':
                        {u'coordinates':
                             {u'lat': -111.1111, u'long': -111.1111}
                         },
                    u'title': u"Pepito's Location" / u"Pinned Location",
                    u'type': u'location',
                    u'url': u'url de la ubicacion en el mapa bing de facebook'
                    }],
              u'mid': u'mid.1478:3043c38',
              u'seq': 266},
         u'recipient': {u'id': u'1049948'},
         u'sender': {u'id': u'1310523468'},
         u'timestamp': 1478998120436
     }
    """
    answer = answer.get("message", {}).get("attachments", {})

    response = {"answer_ok": False,
                "data": "No recibí tu ubicación",
                "msg_return_type": "location"}

    if answer:
        for payload in answer:
            coordinates = payload.get('payload', {}).get('coordinates', {})
            if coordinates.get('lat', None) and coordinates.get('long', None):
                # verificar si las coordenadas pertenecen al usuario o fueron fijadas por él.
                if payload.get("title", "").rfind("Pinned") == -1:
                    response["answer_ok"] = True
                    response["data"] = coordinates
                else:
                    response["answer_ok"] = False
                    response["data"] = u"Parece que haz seleccionado otra ubicación"
                return response

    return response


def process_text_answer(answer):
    """
    Verificar y procesar la respuesta de texto del usuario
    :param answer:
    :return: dict
    Formato del json que retorna facebook con la respuesta de mensaje de texto:
    {
        u'message':
             {u'text': 'hello, world'},
         u'recipient': {u'id': u'1049948'},
         u'sender': {u'id': u'1310523468'},
         u'timestamp': 1478998120436
     }
     """

    response = {"answer_ok": False, "data": "No entiendo :(", "msg_return_type": "text"}
    if answer.get("message", {}).get("text", {}) and not (answer.get("message", {}).get("quick_replies", {})):
        response["answer_ok"] = True
        response["data"] = answer["message"]["text"]
    return response


def process_quick_replies_answer(answer, avaiblable_options):

    """Formato del json que retorna facebook con la respuesta del quick_replies del usuario:
        {
        u'message':
             {u'text': u'texto del boton seleccionado'
             {u'quick_replies':
                {u'payload': u'Identificador_definido_para_el_boton_seleccionado'}
              u'mid': u'mid.1478:3043c38',
              u'seq': 266},
         u'recipient': {u'id': u'1049948'},
         u'sender': {u'id': u'1310523468'},
         u'timestamp': 1478998120436
     }
     """
    answer_key = answer.get("message", {}).get("quick_reply", {}).get("payload", {})

    response = {"answer_ok": False,
                "data": "Por favor selecciona una opcion para continuar",
                "msg_return_type": "text"}

    if answer_key:
        if answer_key in avaiblable_options:
            response["answer_ok"] = True
            response["data"] = answer_key

    return response


def send_question_repeat_survey(request, bot, client, survey):
    fb_user_id = client.facebook_user_id
    if survey.company.identifier == 'SQ':
        question_repeat = ("Ya respondiste la pregunta :D . Tú tambien puedes crear "
        "y compartir tu propia pregunta en {0}/scory/survey, revisa los votos "
        "en {0}/scory/result/{1}".format(request.get_host(), survey.name))
        message = Message(survey = survey, client=client, bot_message = question_repeat)
        message.save()
        bot.send_text_message(fb_user_id, question_repeat)
    else:
        quick_replies_labels = list()
        question_repeat = "¿Desea responder nuevamente la encuesta (%s)?" % survey.name
        quick_reply_yes = QuickReplyLabel('Si', 'repeat_survey/yes')
        quick_reply_no = QuickReplyLabel('No', 'repeat_survey/no')
        quick_replies_labels.append(quick_reply_yes)
        quick_replies_labels.append(quick_reply_no)
        message = Message(survey = survey, client=client, bot_message = question_repeat)
        message.save()
        bot.send_quick_replies_labels(fb_user_id, question_repeat, quick_replies_labels)


def send_message_with_question_type_format(bot, fb_user_id, survey, message_text=None, message_type=None, options_answer=[], expired_survey=False):

    if isinstance(bot, Bot):
        # TODO
        # Se debe crear variable globales para la comparacion de los tipos de preguntas

        # set default message
        if expired_survey:
            message_text = MESSAGE_INACTIVITY
            message = Message(survey = survey, bot_message = MESSAGE_INACTIVITY)
            message.save()
        if not message_text:
            message_text = END_MESSAGE_SURVEY
            if not survey.end_message == '':
                message_text = survey.end_message
            message = Message(survey = survey, bot_message = message_text)
            message.save()
        if not message_type:
            message_type = "text"

        # bot stop typing
        bot.send_action(fb_user_id, "typing_off")
        if message_type == u'location':
            bot.send_quick_replies_location(fb_user_id, message_text)
        elif message_type == u'multi_option':
            # check if it has options answer
            if options_answer:
                #options_answer = question.option_question.options_answer
                quick_replies_labels = list()
                for opt_ans in options_answer:
                    quick_reply = QuickReplyLabel(opt_ans.name, opt_ans.key)
                    if opt_ans.url_img:
                        quick_reply.image_url = opt_ans.url_img
                    quick_replies_labels.append(quick_reply)
                bot.send_quick_replies_labels(fb_user_id, message_text, quick_replies_labels)
            else:
                # TODO bot should handle an error response in this case
                logging.error("question_type multi_option should have options_answer")
        elif message_type == u'text':
            bot.send_text_message(fb_user_id, message_text)


def send_next_question(bot, next_option_question, client, survey):
    if next_option_question:
        next_question = Question.objects.create(option_question=next_option_question, answer="", client=client)
        if next_question.option_question.options_answer:
            options_answer = list(next_question.option_question.options_answer.all())
        send_message_with_question_type_format(bot,
                                               fb_user_id=client.facebook_user_id,
                                               survey=survey,
                                               message_text=next_question.option_question.question_text,
                                               message_type=next_question.option_question.type.name,
                                               options_answer=options_answer)
    else:
        # send the default message
        send_message_with_question_type_format(bot, fb_user_id=client.facebook_user_id, survey=survey)

        

def re_send_current_question(bot, question, client, response, survey, options_answer=[]):
    """
     Send the current question again with a previous response message
    :param bot: Bot
    :param question: current question
    :param client: Client
    :param response: Response message
    :param options_answer: OptionsAnswer in case of quick_reply
    :return:
    """
    # response message
    send_message_with_question_type_format(bot,
                                           survey=survey,
                                           fb_user_id=client.facebook_user_id,
                                           message_text=response["data"],
                                           message_type=response["msg_return_type"])
    # current question
    send_message_with_question_type_format(bot,
                                           survey=survey,
                                           fb_user_id=client.facebook_user_id,
                                           message_text=question.option_question.question_text,
                                           message_type=question.option_question.type.name,
                                           options_answer=options_answer)

def skip_question(bot, client, survey, question, sequence):
    question.answer = 'No contestada'
    question.save()
    next_option_question = survey.get_next_question(question.option_question, sequence)
    send_next_question(bot, next_option_question, client, survey)


def process_answer(request, client, answer, bot, survey, start_survey=False):
    # TODO se debe mejorar la opcion para identificar si se debe comenzar con la primera pregunta de la encuesta

    if 'message' in answer and 'quick_reply' in answer['message']:
        answer_repeat_survey = answer.get("message", {}).get("quick_reply", {}).get("payload", {})
    
        if answer_repeat_survey == 'repeat_survey/no':
            message = Message(survey = survey, client=client, user_message = 'No')
            message.save()
            bot.send_text_message(client.facebook_user_id, 'Ok :)')
            message = Message(survey = survey, bot_message = "Ok :)")
            message.save()

            print 'Encuesta finalizada'
            return
        elif answer_repeat_survey == 'repeat_survey/yes':
            message = Message(survey = survey, client=client, user_message = 'Si')
            message.save()
            if survey.start_message != "":
                send_message_with_question_type_format(bot,
                                                       fb_user_id=client.facebook_user_id,
                                                       survey=survey,
                                                       message_text=survey.start_message)
                message = Message(survey = survey, client=client, bot_message = survey.start_message)
                message.save()
            next_option_question = survey.get_next_question()
            send_next_question(bot, next_option_question, client, survey)
            return

    if not start_survey:
        question = Question.objects.filter(option_question__survey=survey, client_id=client, answer="").last()
        start_survey = not question

    if not survey:
        send_message_with_question_type_format(bot,
                                               fb_user_id=client.facebook_user_id,
                                               survey=survey,
                                               message_text=SURVEY_NOT_FOUND)
    else:
        # TODO there are code very similar for each type, so maybe we need to refactor this method
     
        if start_survey:
            last_question_answered = Question.objects.filter(option_question__survey=survey, client_id=client).exclude(answer__isnull=True).exclude(answer=u'').last()
            if last_question_answered:
                if "message" in answer and "text" in answer["message"]:
                    message = Message(survey = survey, client=client, user_message = answer['message']["text"])
                    message.save()
                else:
                    message = Message(survey = survey, client=client, user_message = "undefined")
                    message.save()
                    message = Message(survey = survey, client=client, bot_message = "Error: Mensaje no permitido")
                    message.save()
                send_question_repeat_survey(request, bot,client,survey)
            else:
                if survey.start_message != "":
                    send_message_with_question_type_format(bot,
                                                           fb_user_id=client.facebook_user_id,
                                                           survey=survey,
                                                           message_text=survey.start_message)
                    message = Message(survey = survey, client=client, bot_message = survey.start_message)
                    message.save()
                next_option_question = survey.get_next_question()
                send_next_question(bot, next_option_question, client, survey)
        else:
            sequence = None
            if not question.option_question.sequence:
                # Para los casos en que la pregunta no tiene secuencia por ser una dependiente se debe asignar la secuencia
                # de la ultima pregunta respondida
                last_question_answered = Question.objects.filter(option_question__survey=survey, client_id=client).exclude(answer__isnull=True).exclude(answer=u'').last()
                if last_question_answered:
                    sequence = last_question_answered.option_question.sequence

            # TODO Hay que identificar el inicio de una encuesta de otra forma
            if question.option_question.type.name == u'location':
                response = process_location_answer(answer)
                if response["answer_ok"]:
                    if not time_inactivity_survey(survey, question, bot, client):
                        question.answer = response["data"]
                        question.save()
                        message = Message(survey = survey, client=client, question=question)
                        message.save()
                        next_option_question = survey.get_next_question(question.option_question, sequence)
                        send_next_question(bot, next_option_question, client, survey)
                else:
                    if "text" in answer["message"]:
                        if answer["message"]["text"].lower() == JUMP_QUESTION:
                            if not question.option_question.mandatory:
                                skip_question(bot, client, survey, question, sequence)
                            else:
                                bot.send_text_message(client.facebook_user_id, "Disculpe, esta pregunta es obligatoria")
                                re_send_current_question(bot, question, client, response, survey)
                        else:
                            if question.client.first_name != 'undefined':
                                message = Message(survey = survey, client=client, bot_message = question.option_question)
                                message.save()
                                message = Message(survey = survey, client=client, user_message = answer["message"]["text"])
                                message.save()
                            re_send_current_question(bot, question, client, response, survey)
                    else:
                        message = Message(survey = survey, client=client, bot_message = question.option_question)
                        message.save()
                        message = Message(survey = survey, client=client, user_message = "undefined")
                        message.save()
                        message = Message(survey = survey, client=client, bot_message = "Error: Mensaje no permitido")
                        message.save()
                        re_send_current_question(bot, question, client, response, survey)

            elif question.option_question.type.name == u'multi_option':
                available_options = question.option_question.options_answer.values_list("key", flat=True)
                response = process_quick_replies_answer(answer, available_options)
                if response["answer_ok"]:
                    if not time_inactivity_survey(survey, question, bot, client):
                        question.answer = response["data"]
                        question.save()
                        message = Message(survey = survey, question=question)
                        message.save()
                        option_answer = question.option_question.options_answer.filter(key=question.answer).first()
                        if option_answer.trigger_question:
                            next_option_question = option_answer.trigger_question
                        else:
                            next_option_question = survey.get_next_question(question.option_question, sequence)

                        send_next_question(bot, next_option_question, client, survey)
                else:
                    if "text" in answer["message"]:
                        if answer["message"]["text"].lower() == JUMP_QUESTION:
                            if not question.option_question.mandatory:
                                skip_question(bot, client, survey, question, sequence)
                            else:
                                bot.send_text_message(client.facebook_user_id, "Disculpe, esta pregunta es obligatoria")
                                options_answer = list(question.option_question.options_answer.all())
                                re_send_current_question(bot, question, client, response, survey, options_answer=options_answer)
                        else:
                            if question.client.first_name != 'undefined':
                                message = Message(survey = survey, client=client, bot_message = question.option_question)
                                message.save()
                                message = Message(survey = survey, client=client, user_message = answer["message"]["text"])
                                message.save()
                                message = Message(survey = survey, client=client, bot_message = "Por favor selecciona una opcion para continuar")
                                message.save()
                            options_answer = list(question.option_question.options_answer.all())
                            re_send_current_question(bot, question, client, response, survey, options_answer=options_answer)
                    else:
                        message = Message(survey = survey, client=client, bot_message = question.option_question)
                        message.save()
                        message = Message(survey = survey, client=client, user_message = "undefined")
                        message.save()
                        message = Message(survey = survey, client=client, bot_message = "Error: Mensaje no permitido")
                        message.save()
                        options_answer = list(question.option_question.options_answer.all())
                        re_send_current_question(bot, question, client, response, survey, options_answer=options_answer)

            elif question.option_question.type.name == u'text':
                response = process_text_answer(answer)
                if response["answer_ok"]:
                    if not time_inactivity_survey(survey, question, bot, client):
                        if response["data"].lower() == JUMP_QUESTION:
                            if not question.option_question.mandatory:
                                skip_question(bot, client, survey, question, sequence)
                            else:
                                bot.send_text_message(client.facebook_user_id, "Disculpe, esta pregunta es obligatoria")
                                send_message_with_question_type_format(bot,
                                                                       fb_user_id=client.facebook_user_id,
                                                                       survey=survey,
                                                                       message_text=question.option_question.question_text,
                                                                       message_type=question.option_question.type.name)
                        else: 
                            question.answer = response["data"]
                            question.save()
                            message = Message(survey = survey, question=question)
                            message.save()
                            next_option_question = survey.get_next_question(question.option_question, sequence)
                            send_next_question(bot, next_option_question, client, survey)
                else:
                    message = Message(survey = survey, client=client, bot_message = question.option_question)
                    message.save()
                    message = Message(survey = survey, client=client, user_message = "undefined")
                    message.save()
                    message = Message(survey = survey, client=client, bot_message = "Error: Mensaje no permitido")
                    message.save()
                    re_send_current_question(bot, question, client, response, survey)
            else:
                message = Message(survey = survey, client=client, user_message = question.option_question, bot_message = "Error: Mensaje no permitido")
                message.save()
                message = Message(survey = survey, client=client, user_message = "undefined")
                message.save()
                message = Message(survey = survey, client=client, bot_message = "Error: Mensaje no permitido")
                message.save()
                bot.send_text_message(client.facebook_user_id, "No entiendo, perdon :(")


def time_inactivity_survey(survey, question, bot, client):
    if survey.get_minute_inactivity_survey(question):
        question.answer = "Tiempo de inactividad cumplido"
        question.save()
        send_message_with_question_type_format(bot, fb_user_id=client.facebook_user_id, survey=survey, expired_survey=True)
        return True
    return False


"""
Funciones para validar la estructura del tipo de pregunta
"""
QUESTION_TYPE = ["location", "text", "multi_option"] # estos datos seran creados como datos semillas en la bd
def validate_answer_type(answer, question_type):
    """
    Verifica si el answer recibido posee la estructura correspondiente al question_type
    :param question_type
    :param answer:
    :return:
    """
    if question_type == u'location':
        validate_answer_type_location(answer)
    elif question_type == u'text':
        validate_answer_type_text()
    elif question_type == u'multi_option' or question_type == u'rating':
        validate_answer_type_quick_replies()

    return False


def validate_answer_type_location(answer):
    """
    Verificar si la estructura del diccionario answer es de tipo location
    :param answer:
    :return:
    Formato del json que retorna facebook con la respuesta de la localizacion del usuario:
        {
        u'message':
             {u'attachments':
                  [{u'payload':
                        {u'coordinates':
                             {u'lat': -111.1111, u'long': -111.1111}
                         },
                    u'title': u"Pepito's Location" / u"Pinned Location",
                    u'type': u'location',
                    u'url': u'url de la ubicacion en el mapa bing de facebook'
                    }],
              u'mid': u'mid.1478:3043c38',
              u'seq': 266},
         u'recipient': {u'id': u'1049948'},
         u'sender': {u'id': u'1310523468'},
         u'timestamp': 1478998120436
     }
     """
    answer = answer.get("message", {}).get("attachments", {})

    if answer:
        for payload in answer:
            if payload.get('type') == u'location':
                return True

    return False


def validate_answer_type_text(answer):
    """
    Verificar si la estructura del diccionario answer es de tipo location
    :param answer:
    :return:
    Formato del json que retorna facebook con la respuesta de mensaje de texto:
    {
        u'message':
             {u'text': 'hello, world'},
         u'recipient': {u'id': u'1049948'},
         u'sender': {u'id': u'1310523468'},
         u'timestamp': 1478998120436
     }
     """

    if answer.get("message", {}).get("text", {}) and not (answer.get("message", {}).get("quick_replies", {})):
        return True
    return False


def validate_answer_type_quick_replies(answer):
    """
    Verificar si la estructura del diccionario answer es de tipo quick_replies
    :param answer:
    :return:
    Formato del json que retorna facebook con la respuesta de la localizacion del usuario:
        {
        u'message':
             {u'text': u'texto del boton seleccionado'
             {u'quick_replies':
                {u'payload': u'Identificador definido para el boton seleccionado'}
              u'mid': u'mid.1478:3043c38',
              u'seq': 266},
         u'recipient': {u'id': u'1049948'},
         u'sender': {u'id': u'1310523468'},
         u'timestamp': 1478998120436
     }
     """
    if answer.get("message", {}).get("text", {}) and \
            answer.get("message", {}).get("quick_replies", {}).get("payload", {}):
        return True
    return False

"""# funcion para enviar messajes al usuario sin usar el packete pymessenger
def post_facebook_message(message_to_send, token):
    post_message_url = 'https://graph.facebook.com/v2.6/me/messages?access_token=%s'%token
    resp = requests.post(post_message_url, headers={"Content-Type": "application/json"},data=message_to_send)"""

def index_api_maps(request):
   return render_to_response('api_maps.html')


class UrlCodeMessenger(APIView):

    def post(self, request, format=None):
        survey_id = request.data.get('survey_id',None)
        obj_bot_setting = BotSetting.objects.first()
        obj = ManageThreadSettingsApi(obj_bot_setting.token)

        #encrypting params survey_id
        param = str(survey_id)
        param_encrypt = 'scory' + str(random.randint(111, 119)*random.randint(1, 8)) + param + str(random.randint(111, 119)*random.randint(1, 8)) 

        url_code_messenger = obj.get_url_image_code_by_param(param_encrypt, 1000)
        return Response({'url_code_messenger': url_code_messenger['uri'], 'status': status.HTTP_200_OK})