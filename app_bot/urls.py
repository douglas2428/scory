from django.conf.urls import url
from django.views.generic import TemplateView
from .views import FacebookMessenger
from app_bot import views

urlpatterns = [
    url(r'^gateway$', FacebookMessenger.as_view(), name='bot'),
    url(r'^googleMaps', views.index_api_maps, name='index'),
    url(r'^privacy_policy/$', TemplateView.as_view(template_name="privacy_policy.html")),
    url(r'^url_code_messenger/$', views.UrlCodeMessenger.as_view(), name='url_code_messenger'),
]
