from django import template
from alfredbot.settings.prod import STATIC_FILES_VERSION

register = template.Library()


@register.simple_tag
def get_version_identifier():
    return STATIC_FILES_VERSION